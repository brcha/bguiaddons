/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of bguiaddons
 */

#ifndef BOPENGLPRIMITIVERENDERER_H
#define BOPENGLPRIMITIVERENDERER_H

#include <bguiaddons_export.h>

#include <QtCore/QObject>
#include <QtCore/QHash>
#include <QtCore/QVector>
#include <QtGui/QMatrix4x4>
#include <QtGui/QVector3D>
#include <QtGui/QColor>

#include "bopenglshadermanager.h"

class QOpenGLContext;

class BOpenGLPrimitiveInstancingManager;
class BOpenGLAbstractPrimitive;
class BOpenGLCamera;

typedef int BOpenGLPrimitiveID;

class BGUIADDONS_EXPORT BOpenGLPrimitiveRenderer : public QObject
{
    Q_OBJECT
public:
    explicit BOpenGLPrimitiveRenderer(QObject *parent = 0);
    ~BOpenGLPrimitiveRenderer();

    // Instanced primitives
    void setInstancingManager(BOpenGLPrimitiveInstancingManager* manager);
    void addInstancedPrimitive(const BOpenGLPrimitiveID id
                              ,BOpenGLAbstractPrimitive* primitive);
    void removeInstancedPrimitive(const BOpenGLPrimitiveID id);
    void allocateInstancedPrimitive(const BOpenGLPrimitiveID id
                                   ,const int size);
    void setInstancedMatrix(const BOpenGLPrimitiveID id
                           ,const int idx
                           ,const QMatrix4x4& matrix);
    void setInstancedColor(const BOpenGLPrimitiveID id
                          ,const int idx
                          ,const QColor& color);
    void setInstancedColor(const BOpenGLPrimitiveID id
                          ,const int idx
                          ,const QVector3D& color);
    void setInstancedMatrices(const BOpenGLPrimitiveID id
                             ,const QVector<QMatrix4x4>& matrices);
    void setInstancedColors(const BOpenGLPrimitiveID id
                           ,const QVector<QColor>& colors);
    void setInstancedColors(const BOpenGLPrimitiveID id
                           ,const QVector<QVector3D>& colors);

    // Single primitives
    void addSinglePrimitive(const BOpenGLPrimitiveID id
                           ,BOpenGLAbstractPrimitive* primitive);
    void removeSinglePrimitive(const BOpenGLPrimitiveID id);
    void setSingleMatrix(const BOpenGLPrimitiveID id
                        ,const QMatrix4x4& matrix);
    void setSingleColor(const BOpenGLPrimitiveID id
                       ,const QColor& color);
    void setSingleColor(const BOpenGLPrimitiveID id
                       ,const QVector3D& color);

    // OpenGL
    void render(const BOpenGLCamera* camera);
    BOpenGLShaderManagerPtr instancingShader() const;
    BOpenGLShaderManagerPtr singleShader(BOpenGLPrimitiveID id) const;

private:
    BOpenGLPrimitiveInstancingManager * m_instancingManager;
    QHash<BOpenGLPrimitiveID, int> m_primitiveIndexHash;
    QHash<BOpenGLPrimitiveID, QVector<QMatrix4x4>> m_instancedModelMatrices;
    QHash<BOpenGLPrimitiveID, QVector<QVector3D>> m_instancedColors;

    QHash<BOpenGLPrimitiveID, BOpenGLAbstractPrimitive*> m_singlePrimitives;
    QHash<BOpenGLPrimitiveID, QMatrix4x4> m_singlePrimitiveModelMatrix;
    QHash<BOpenGLPrimitiveID, QVector3D> m_singlePrimitiveColor;
};

#endif // BOPENGLPRIMITIVERENDERER_H
