/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2014, 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of bguiaddons
 */

#include "bopenglshadermanager.h"

#include <QtGui/QOpenGLContext>
#include <QtGui/QOpenGLShaderProgram>

BOpenGLShaderManager::BOpenGLShaderManager()
    : m_shader(new QOpenGLShaderProgram)
{
}

BOpenGLShaderManager::~BOpenGLShaderManager()
{
    m_shader->release();
}

void BOpenGLShaderManager::bind()
{
    m_shader->bind();
    foreach (const GLuint unit, m_unitConfigs.keys()) {
        const BOpenGLTextureUnitConfiguration& config = m_unitConfigs.value(unit);

        // Bind the texture
        config.texture()->bind(unit);

        // Bind the sampler if it is present
        if (config.sampler())
            config.sampler()->bind(unit);

        // Associate with sampler uniform in shader if name is given
        if (m_samplerUniforms.contains(unit))
            m_shader->setUniformValue(m_samplerUniforms.value(unit).constData(), unit);
    }
}

void BOpenGLShaderManager::setShaders(const QString& vertexShader, const QString& fragmentShader)
{
    if (!m_shader->addShaderFromSourceFile(QOpenGLShader::Vertex, vertexShader))
        qCritical() << QObject::tr( "Could not compile vertex shader. Log:" ) << m_shader->log();

    if (!m_shader->addShaderFromSourceFile(QOpenGLShader::Fragment, fragmentShader))
        qCritical() << QObject::tr( "Could not compile fragment shader. Log:" ) << m_shader->log();

    if (!m_shader->link())
        qCritical() << QObject::tr( "Could not link shader program. Log:" ) << m_shader->log();
}

#if !defined(QT_OPENGL_ES_2)
void BOpenGLShaderManager::setShaders(const QString& vertexShader, const QString& geometryShader, const QString& fragmentShader)
{
    if (!m_shader->addShaderFromSourceFile(QOpenGLShader::Vertex, vertexShader))
        qCritical() << QObject::tr( "Could not compile vertex shader. Log:" ) << m_shader->log();

    if (!m_shader->addShaderFromSourceFile(QOpenGLShader::Geometry, geometryShader))
        qCritical() << QObject::tr( "Could not compile geometry shader. Log:" ) << m_shader->log();

    if (!m_shader->addShaderFromSourceFile(QOpenGLShader::Fragment, fragmentShader))
        qCritical() << QObject::tr( "Could not compile fragment shader. Log:" ) << m_shader->log();

    if (!m_shader->link())
        qCritical() << QObject::tr( "Could not link shader program. Log:" ) << m_shader->log();
}

void BOpenGLShaderManager::setShaders(const QString& vertexShader
                                         ,const QString& tesselationControlShader
                                         ,const QString& tesselationEvaluationShader
                                         ,const QString& geometryShader
                                         ,const QString& fragmentShader)
{
    if (!m_shader->addShaderFromSourceFile(QOpenGLShader::Vertex, vertexShader))
        qCritical() << QObject::tr( "Could not compile vertex shader. Log:" ) << m_shader->log();

    if (!m_shader->addShaderFromSourceFile(QOpenGLShader::TessellationControl, tesselationControlShader))
        qCritical() << QObject::tr( "Could not compile tesselation control shader. Log:" ) << m_shader->log();

    if (!m_shader->addShaderFromSourceFile(QOpenGLShader::TessellationEvaluation, tesselationEvaluationShader))
        qCritical() << QObject::tr( "Could not compile tesselation evaluation shader. Log:" ) << m_shader->log();

    if (!m_shader->addShaderFromSourceFile(QOpenGLShader::Geometry, geometryShader))
        qCritical() << QObject::tr( "Could not compile geometry shader. Log:" ) << m_shader->log();

    if (!m_shader->addShaderFromSourceFile(QOpenGLShader::Fragment, fragmentShader))
        qCritical() << QObject::tr( "Could not compile fragment shader. Log:" ) << m_shader->log();

    if (!m_shader->link())
        qCritical() << QObject::tr( "Could not link shader program. Log:" ) << m_shader->log();
}

#endif /* !defined(QT_OPENGL_ES_2) */

void BOpenGLShaderManager::setShader(const QOpenGLShaderProgramPtr& shader)
{
    m_shader = shader;
}

QOpenGLShaderProgramPtr BOpenGLShaderManager::shader() const
{
    return m_shader;
}

void BOpenGLShaderManager::setTextureUnitConfiguration(GLuint unit, QOpenGLTexturePtr texture, BOpenGLSamplerPtr sampler)
{
    BOpenGLTextureUnitConfiguration config(texture, sampler);
    m_unitConfigs.insert(unit, config);
}

void BOpenGLShaderManager::setTextureUnitConfiguration(GLuint unit, QOpenGLTexturePtr texture, BOpenGLSamplerPtr sampler
                                                                     ,const QByteArray& uniformName)
{
    setTextureUnitConfiguration(unit, texture, sampler);
    m_samplerUniforms.insert(unit, uniformName);
}

void BOpenGLShaderManager::setTextureUnitConfiguration(GLuint unit, QOpenGLTexturePtr texture)
{
    BOpenGLSamplerPtr sampler;
    setTextureUnitConfiguration(unit, texture, sampler);
}

void BOpenGLShaderManager::setTextureUnitConfiguration(GLuint unit, QOpenGLTexturePtr texture, const QByteArray& uniformName)
{
    BOpenGLSamplerPtr sampler;
    setTextureUnitConfiguration(unit, texture, sampler, uniformName);
}

BOpenGLTextureUnitConfiguration BOpenGLShaderManager::textureUnitConfiguration(GLuint unit) const
{
    return m_unitConfigs.value(unit, BOpenGLTextureUnitConfiguration());
}
