/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2014, 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of bguiaddons
 */

#ifndef BOPENGLCAMERA_P_H
#define BOPENGLCAMERA_P_H

#include <QMatrix4x4>
#include <QVector3D>

class BOpenGLCameraPrivate {
public:
    BOpenGLCameraPrivate(BOpenGLCamera * qq)
        : q_ptr(qq)
        , m_position(0.0f, 0.0f, 1.0f)
        , m_upVector(0.0f, 1.0f, 0.0f)
        , m_viewCenter(0.0f, 0.0f, 0.0f)
        , m_cameraToCenter(0.0f, 0.0f, -1.0f)
        , m_projectionType(BOpenGLCamera::OrthogonalProjection)
        , m_nearPlane(0.1f)
        , m_farPlane(1024.0f)
        , m_fieldOfView(60.0f)
        , m_aspectRatio(1.0f)
        , m_left(-0.5f)
        , m_right(0.5f)
        , m_bottom(-0.5f)
        , m_top(0.5f)
        , m_inQml(false)
        , m_viewMatrixDirty(true)
        , m_viewProjectionMatrixDirty(true)
    {
        updateOrthogonalProjection();
    }

    ~BOpenGLCameraPrivate()
    {
    }

    inline void updatePerspectiveProjection()
    {
        m_projectionMatrix.setToIdentity();
        m_projectionMatrix.perspective(m_fieldOfView, m_aspectRatio, m_nearPlane, m_farPlane);
        if (m_inQml)
            m_projectionMatrix.scale(1.0f, -1.0f, 1.0f); // flip y axis
        m_viewProjectionMatrixDirty = true;
    }

    inline void updateOrthogonalProjection()
    {
        m_projectionMatrix.setToIdentity();
        m_projectionMatrix.ortho(m_left, m_right, m_bottom, m_top, m_nearPlane, m_farPlane);
        if (m_inQml)
            m_projectionMatrix.scale(1.0f, -1.0f, 1.0f); // flip y axis
        m_viewProjectionMatrixDirty = true;
    }

    Q_DECLARE_PUBLIC(BOpenGLCamera)

    BOpenGLCamera * q_ptr;

    QVector3D m_position;
    QVector3D m_upVector;
    QVector3D m_viewCenter;

    QVector3D m_cameraToCenter;

    BOpenGLCamera::ProjectionType m_projectionType;

    float m_nearPlane;
    float m_farPlane;

    float m_fieldOfView;
    float m_aspectRatio;

    float m_left;
    float m_right;
    float m_bottom;
    float m_top;

    bool m_inQml;

    mutable QMatrix4x4 m_viewMatrix;
    mutable QMatrix4x4 m_projectionMatrix;
    mutable QMatrix4x4 m_viewProjectionMatrix;

    mutable bool m_viewMatrixDirty;
    mutable bool m_viewProjectionMatrixDirty;
};

#endif // BOPENGLCAMERA_P_H

