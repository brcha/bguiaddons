/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2014, 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of bguiaddons
 */

#ifndef BOPENGLCAMERA_H
#define BOPENGLCAMERA_H

#include <bguiaddons_export.h>

#include <QtCore/QObject>
#include <QtGui/QMatrix4x4>
#include <QtGui/QQuaternion>
#include <QtGui/QVector3D>
#include <QtCore/QSharedPointer>

class BOpenGLCameraPrivate;

class QOpenGLShaderProgram;
typedef QSharedPointer<QOpenGLShaderProgram> QOpenGLShaderProgramPtr;

class BGUIADDONS_EXPORT BOpenGLCamera : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QVector3D position READ position WRITE setPosition)
    Q_PROPERTY(QVector3D upVector READ upVector WRITE setUpVector)
    Q_PROPERTY(QVector3D viewCenter READ viewCenter WRITE setViewCenter)

    Q_PROPERTY(ProjectionType projectionType READ projectionType)
    Q_PROPERTY(float nearPlane READ nearPlane WRITE setNearPlane)
    Q_PROPERTY(float farPlane READ farPlane WRITE setFarPlane)

    Q_PROPERTY(float fieldOfView READ fieldOfView WRITE setFieldOfView)
    Q_PROPERTY(float aspectRatio READ aspectRatio WRITE setAspectRatio)

    Q_PROPERTY(float left READ left WRITE setLeft)
    Q_PROPERTY(float right READ right WRITE setRight)
    Q_PROPERTY(float bottom READ bottom WRITE setBottom)
    Q_PROPERTY(float top READ top WRITE setTop)

    Q_PROPERTY(bool inQml READ inQml WRITE setInQml)

    Q_ENUMS(ProjectionType)

public:
    enum ProjectionType {
        OrthogonalProjection,
        PerspectiveProjection
    };

    enum CameraTranslationOption {
        TranslateViewCenter,
        DontTranslateViewCenter
    };

public:
    explicit BOpenGLCamera(QObject *parent = 0);

    QVector3D position() const;
    QVector3D upVector() const;
    QVector3D viewCenter() const;

    QVector3D viewVector() const;

    ProjectionType projectionType() const;

    void setOrthographicProjection(float left, float right
                                                       ,float bottom, float top
                                                       ,float nearPlane, float farPlane);
    void setPerspectiveProjection(float fieldOfView, float aspect
                                                     ,float nearPlane, float farPlane);

    void setNearPlane(const float & nearPlane);
    float nearPlane() const;

    void setFarPlane(const float & farPlane);
    float farPlane() const;

    void setFieldOfView(const float & fieldOfView);
    float fieldOfView() const;

    void setAspectRatio(const float & aspectRatio);
    float aspectRatio() const;

    void setLeft(const float & left);
    float left() const;

    void setRight(const float & right);
    float right() const;

    void setBottom(const float & bottom);
    float bottom() const;

    void setTop(const float & top);
    float top() const;

    void setInQml(bool inQml);
    bool inQml() const;

    QMatrix4x4 viewMatrix() const;
    QMatrix4x4 projectionMatrix() const;
    QMatrix4x4 viewProjectionMatrix() const;

    QQuaternion tiltRotation(const float & angle) const;
    QQuaternion panRotation(const float & angle) const;
    QQuaternion rollRotation(const float & angle) const;

    /**
     * @brief setStandardUniforms - sets the standard uniforms on the shader program
     *
     * Uniforms that are set are: "mvp", "modelViewMatrix", "normalMatrix" and "projectionMatrix"
     *
     * @param program
     * @param model
     */
    void setStandardUniforms(const QOpenGLShaderProgramPtr& program, const QMatrix4x4& model) const;

public slots:
    void setPosition(const QVector3D& position);
    void setUpVector(const QVector3D& upVector);
    void setViewCenter(const QVector3D& viewCenter);

    void resetToIdentity();

    // Translate in camera space
    void translate(const QVector3D& vLocal, CameraTranslationOption option = TranslateViewCenter);

    // Translate in world space
    void translateWorld(const QVector3D& vWorld, CameraTranslationOption option = TranslateViewCenter);

    void tilt(const float & angle);
    void pan(const float & angle);
    void roll(const float & angle);

    void tiltAboutViewCenter(const float & angle);
    void panAboutViewCenter(const float & angle);
    void rollAboutViewCenter(const float & angle);

    void rotate(const QQuaternion& q);
    void rotateAboutViewCenter(const QQuaternion& q);

protected:
    Q_DECLARE_PRIVATE(BOpenGLCamera)

private:
    BOpenGLCameraPrivate * d_ptr;
};

typedef QSharedPointer<BOpenGLCamera> BOpenGLCameraPtr;

#endif // BOPENGLCAMERA_H
