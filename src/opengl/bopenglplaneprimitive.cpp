/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2014, 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of bguiaddons
 */

#include "bopenglplaneprimitive.h"

BOpenGLPlanePrimitive::BOpenGLPlanePrimitive(QObject *parent)
    : BOpenGLAbstractPrimitive(parent)
    , m_width(1.0f)
    , m_height(1.0f)
    , m_xDivisions(1)
    , m_yDivisions(1)
{   
}

BOpenGLPlanePrimitive::BOpenGLPlanePrimitive(float width, float height, int xDivisions, int yDivisions, QObject* parent)
    : BOpenGLAbstractPrimitive(parent)
    , m_width(width)
    , m_height(height)
    , m_xDivisions(xDivisions)
    , m_yDivisions(yDivisions)
{
}

BOpenGLPlanePrimitive::~BOpenGLPlanePrimitive()
{

}

float BOpenGLPlanePrimitive::width() const
{
    return m_width;
}

float BOpenGLPlanePrimitive::height() const
{
    return m_height;
}

int BOpenGLPlanePrimitive::xDivisions() const
{
    return m_xDivisions;
}

int BOpenGLPlanePrimitive::yDivisions() const
{
    return m_yDivisions;
}

void BOpenGLPlanePrimitive::setWidth(float arg)
{
    m_width = arg;
}

void BOpenGLPlanePrimitive::setHeight(float arg)
{
    m_height = arg;
}

void BOpenGLPlanePrimitive::setXDivisions(int arg)
{
    m_xDivisions = arg;
}

void BOpenGLPlanePrimitive::setYDivisions(int arg)
{
    m_yDivisions = arg;
}

void BOpenGLPlanePrimitive::generateVertexData(QVector<float>& vertices
                              ,QVector<float>& normals
                              ,QVector<float>& texCoords
                              ,QVector<float>& tangents
                              ,QVector<unsigned int>& indices)
{
    vertices.resize(3 * (m_xDivisions + 1) * (m_yDivisions + 1));
    normals.resize(3 * (m_xDivisions + 1) * (m_yDivisions + 1));
    texCoords.resize(2 * (m_xDivisions + 1) * (m_yDivisions + 1));
    tangents.resize(3 * (m_xDivisions + 1) * (m_yDivisions + 1));
    indices.resize(6 * m_xDivisions * m_yDivisions);

    float x2 = m_width / 2.0f;
    float y2 = m_height / 2.0f;
    float iFactor = m_height / static_cast<float>(m_yDivisions);
    float jFactor = m_width / static_cast<float>(m_xDivisions);
    float texi = 1.0f / static_cast<float>(m_yDivisions);
    float texj = 1.0f / static_cast<float>(m_xDivisions);

    int vertexIndex = 0;
    int textureIndex = 0;

    // Generate vertices
    for (int i=0; i <= m_yDivisions; ++i)
    {
        float y = iFactor * static_cast<float>(i) - y2;

        for (int j=0; j <= m_xDivisions; ++j)
        {
            float x = jFactor * static_cast<float>(j) - x2;

            vertices[vertexIndex]   = x;
            vertices[vertexIndex+1] = 0.0f;
            vertices[vertexIndex+2] = y;

            normals[vertexIndex]   = 0.0f;
            normals[vertexIndex+1] = 1.0f;
            normals[vertexIndex+2] = 0.0f;

            tangents[vertexIndex]   = 1.0f;
            tangents[vertexIndex+1] = 0.0f;
            tangents[vertexIndex+2] = 0.0f;

            vertexIndex += 3;

            texCoords[textureIndex]   = static_cast<float>(j) * texi;
            texCoords[textureIndex+1] = static_cast<float>(i) * texj;

            textureIndex += 2;
        }
    }

    // Generate indices
    int index = 0;
    for (int i=0; i < m_yDivisions; ++i)
    {
        unsigned int rowStart = i * (m_xDivisions+1);
        unsigned int nextRowStart = (i+1) * (m_xDivisions+1);

        for (int j=0; j < m_xDivisions; ++j)
        {
            indices[index]   = rowStart + j;
            indices[index+1] = nextRowStart + j;
            indices[index+2] = nextRowStart + j + 1;
            indices[index+3] = rowStart + j;
            indices[index+4] = nextRowStart + j + 1;
            indices[index+5] = rowStart + j + 1;
            index += 6;
        }
    }
}

int BOpenGLPlanePrimitive::indexCount() const
{
    return 6 * m_xDivisions * m_yDivisions;
}


