/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2014, 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of bguiaddons
 */

#ifndef BOPENGLSHADERMANAGER_H
#define BOPENGLSHADERMANAGER_H

#include <bguiaddons_export.h>

#include <QtCore/QMap>
#include <QtCore/QPair>
#include <QtCore/QSharedPointer>
#include <QtGui/QOpenGLShaderProgram>
#include <QtGui/QOpenGLTexture>

#include "bopenglsampler.h"

typedef QSharedPointer<QOpenGLShaderProgram> QOpenGLShaderProgramPtr;
typedef QSharedPointer<QOpenGLTexture> QOpenGLTexturePtr;

class BOpenGLTextureUnitConfiguration : public QPair<QOpenGLTexturePtr, BOpenGLSamplerPtr>
{
public:
    BOpenGLTextureUnitConfiguration()
        : QPair<QOpenGLTexturePtr, BOpenGLSamplerPtr>(QOpenGLTexturePtr(), BOpenGLSamplerPtr())
    {
    }

    explicit BOpenGLTextureUnitConfiguration(const QOpenGLTexturePtr& texture, const BOpenGLSamplerPtr& sampler)
        : QPair<QOpenGLTexturePtr, BOpenGLSamplerPtr>(texture, sampler)
    {
    }

    void setTexture(const QOpenGLTexturePtr& texture)
    {
        first = texture;
    }
    QOpenGLTexturePtr texture() const
    {
        return first;
    }

    void setSampler(const BOpenGLSamplerPtr& sampler)
    {
        second = sampler;
    }
    BOpenGLSamplerPtr sampler() const
    {
        return second;
    }
};

class BGUIADDONS_EXPORT BOpenGLShaderManager
{
public:
    BOpenGLShaderManager();
    ~BOpenGLShaderManager();

    void bind();

    void setShaders(const QString& vertexShader
                              ,const QString& fragmentShader);
#if !defined(QT_OPENGL_ES_2)
    void setShaders(const QString& vertexShader
                   ,const QString& geometryShader
                   ,const QString& fragmentShader);
    void setShaders(const QString& vertexShader
                   ,const QString& tesselationControlShader
                   ,const QString& tesselationEvaluationShader
                   ,const QString& geometryShader
                   ,const QString& fragmentShader);
#endif

    void setShader(const QOpenGLShaderProgramPtr& shader);
    QOpenGLShaderProgramPtr shader() const;

    void setTextureUnitConfiguration(GLuint unit, QOpenGLTexturePtr texture, BOpenGLSamplerPtr sampler);
    void setTextureUnitConfiguration(GLuint unit, QOpenGLTexturePtr texture
                                    ,BOpenGLSamplerPtr sampler, const QByteArray& uniformName);

    void setTextureUnitConfiguration(GLuint unit, QOpenGLTexturePtr texture);
    void setTextureUnitConfiguration(GLuint unit, QOpenGLTexturePtr texture
                                    ,const QByteArray& uniformName);

    BOpenGLTextureUnitConfiguration textureUnitConfiguration(GLuint unit) const;

private:
    // Assume this is our shader program
    QOpenGLShaderProgramPtr m_shader;

    // Map of texture units
    QMap<GLuint, BOpenGLTextureUnitConfiguration> m_unitConfigs;
    QMap<GLuint, QByteArray> m_samplerUniforms;
};

typedef QSharedPointer<BOpenGLShaderManager> BOpenGLShaderManagerPtr;

#endif // BOPENGLSHADERMANAGER_H
