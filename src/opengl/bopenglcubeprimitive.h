/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of bguiaddons
 */

#ifndef BOPENGLCUBE_H
#define BOPENGLCUBE_H

#include <bguiaddons_export.h>

#include "bopenglabstractprimitive.h"

class BGUIADDONS_EXPORT BOpenGLCubePrimitive : public BOpenGLAbstractPrimitive
{
    Q_OBJECT

public:
    explicit BOpenGLCubePrimitive(QObject *parent = 0);
    ~BOpenGLCubePrimitive();

    virtual int indexCount() const;

private:
    virtual void generateVertexData(QVector<float>& vertices
                                   ,QVector<float>& normals
                                   ,QVector<float>& texCoords
                                   ,QVector<float>& tangents
                                   ,QVector<unsigned int>& indices);
};

#endif // BOPENGLCUBE_H
