/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2014, 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of bguiaddons
 */

#include "bopenglcamera.h"
#include "bopenglcamera_p.h"

#include <QtGui/QOpenGLShaderProgram>

BOpenGLCamera::BOpenGLCamera(QObject *parent)
    : QObject(parent)
    , d_ptr(new BOpenGLCameraPrivate(this))
{
}

QVector3D BOpenGLCamera::position() const
{
    Q_D(const BOpenGLCamera);
    return d->m_position;
}

QVector3D BOpenGLCamera::upVector() const
{
    Q_D(const BOpenGLCamera);
    return d->m_upVector;
}

QVector3D BOpenGLCamera::viewCenter() const
{
    Q_D(const BOpenGLCamera);
    return d->m_viewCenter;
}

QVector3D BOpenGLCamera::viewVector() const
{
    Q_D(const BOpenGLCamera);
    return d->m_cameraToCenter;
}

BOpenGLCamera::ProjectionType BOpenGLCamera::projectionType() const
{
    Q_D(const BOpenGLCamera);
    return d->m_projectionType;
}

void BOpenGLCamera::setOrthographicProjection(float left, float right
                                                                  ,float bottom, float top
                                                                  ,float nearPlane, float farPlane)
{
    Q_D(BOpenGLCamera);
    d->m_left = left;
    d->m_right = right;
    d->m_bottom = bottom;
    d->m_top = top;
    d->m_nearPlane = nearPlane;
    d->m_farPlane = farPlane;
    d->m_projectionType = OrthogonalProjection;
    d->updateOrthogonalProjection();
}

void BOpenGLCamera::setPerspectiveProjection(float fieldOfView, float aspect
                                                                ,float nearPlane, float farPlane)
{
    Q_D(BOpenGLCamera);
    d->m_fieldOfView = fieldOfView;
    d->m_aspectRatio = aspect;
    d->m_nearPlane = nearPlane;
    d->m_farPlane = farPlane;
    d->m_projectionType = PerspectiveProjection;
    d->updatePerspectiveProjection();
}

void BOpenGLCamera::setNearPlane(const float& nearPlane)
{
    Q_D(BOpenGLCamera);
    if (qFuzzyCompare(d->m_nearPlane, nearPlane))
        return;
    d->m_nearPlane = nearPlane;
    if (d->m_projectionType == PerspectiveProjection)
        d->updatePerspectiveProjection();
}

float BOpenGLCamera::nearPlane() const
{
    Q_D(const BOpenGLCamera);
    return d->m_nearPlane;
}

void BOpenGLCamera::setFarPlane(const float& farPlane)
{
    Q_D(BOpenGLCamera);
    if (qFuzzyCompare(d->m_farPlane, farPlane))
        return;
    d->m_farPlane = farPlane;
    if (d->m_projectionType == PerspectiveProjection)
        d->updatePerspectiveProjection();
}

float BOpenGLCamera::farPlane() const
{
    Q_D(const BOpenGLCamera);
    return d->m_farPlane;
}

void BOpenGLCamera::setFieldOfView(const float& fieldOfView)
{
    Q_D(BOpenGLCamera);
    if (qFuzzyCompare(d->m_fieldOfView, fieldOfView))
        return;
    d->m_fieldOfView = fieldOfView;
    if (d->m_projectionType == PerspectiveProjection)
        d->updatePerspectiveProjection();
}

float BOpenGLCamera::fieldOfView() const
{
    Q_D(const BOpenGLCamera);
    return d->m_fieldOfView;
}

void BOpenGLCamera::setAspectRatio(const float& aspectRatio)
{
    Q_D(BOpenGLCamera);
    if (qFuzzyCompare(d->m_aspectRatio, aspectRatio))
        return;
    d->m_aspectRatio = aspectRatio;
    if (d->m_projectionType == PerspectiveProjection)
        d->updatePerspectiveProjection();
}

float BOpenGLCamera::aspectRatio() const
{
    Q_D(const BOpenGLCamera);
    return d->m_aspectRatio;
}

void BOpenGLCamera::setLeft(const float& left)
{
    Q_D(BOpenGLCamera);
    if (qFuzzyCompare(d->m_left, left))
        return;
    d->m_left = left;
    if (d->m_projectionType == OrthogonalProjection)
        d->updateOrthogonalProjection();
}

float BOpenGLCamera::left() const
{
    Q_D(const BOpenGLCamera);
    return d->m_left;
}

void BOpenGLCamera::setRight(const float& right)
{
    Q_D(BOpenGLCamera);
    if (qFuzzyCompare(d->m_right, right))
        return;
    d->m_right = right;
    if (d->m_projectionType == OrthogonalProjection)
        d->updateOrthogonalProjection();
}

float BOpenGLCamera::right() const
{
    Q_D(const BOpenGLCamera);
    return d->m_right;
}

void BOpenGLCamera::setBottom(const float& bottom)
{
    Q_D(BOpenGLCamera);
    if (qFuzzyCompare(d->m_bottom, bottom))
        return;
    d->m_bottom = bottom;
    if (d->m_projectionType == OrthogonalProjection)
        d->updateOrthogonalProjection();
}

float BOpenGLCamera::bottom() const
{
    Q_D(const BOpenGLCamera);
    return d->m_bottom;
}

void BOpenGLCamera::setTop(const float& top)
{
    Q_D(BOpenGLCamera);
    if (qFuzzyCompare(d->m_top, top))
        return;
    d->m_top = top;
    if (d->m_projectionType == OrthogonalProjection)
        d->updateOrthogonalProjection();
}

float BOpenGLCamera::top() const
{
    Q_D(const BOpenGLCamera);
    return d->m_top;
}

void BOpenGLCamera::setInQml(bool inQml)
{
    Q_D(BOpenGLCamera);
    if (inQml == d->m_inQml)
        return;
    d->m_inQml = inQml;
    if (d->m_projectionType == OrthogonalProjection)
        d->updateOrthogonalProjection();
    else
        d->updatePerspectiveProjection();
}

bool BOpenGLCamera::inQml() const
{
    Q_D(const BOpenGLCamera);
    return d->m_inQml;
}

QMatrix4x4 BOpenGLCamera::viewMatrix() const
{
    Q_D(const BOpenGLCamera);
    if (d->m_viewMatrixDirty)
    {
        d->m_viewMatrix.setToIdentity();
        d->m_viewMatrix.lookAt(d->m_position, d->m_viewCenter, d->m_upVector);
        d->m_viewMatrixDirty = false;
    }
    return d->m_viewMatrix;
}

QMatrix4x4 BOpenGLCamera::projectionMatrix() const
{
    Q_D(const BOpenGLCamera);
    return d->m_projectionMatrix;
}

QMatrix4x4 BOpenGLCamera::viewProjectionMatrix() const
{
    Q_D(const BOpenGLCamera);
    if (d->m_viewMatrixDirty || d->m_viewProjectionMatrixDirty)
    {
        d->m_viewProjectionMatrix = d->m_projectionMatrix * viewMatrix();
        d->m_viewProjectionMatrixDirty = false;
    }
    return d->m_viewProjectionMatrix;
}

QQuaternion BOpenGLCamera::tiltRotation(const float& angle) const
{
    Q_D(const BOpenGLCamera);
    QVector3D xBasis = QVector3D::crossProduct(d->m_upVector, d->m_cameraToCenter.normalized()).normalized();
    return QQuaternion::fromAxisAndAngle(xBasis, -angle);
}

QQuaternion BOpenGLCamera::panRotation(const float& angle) const
{
    Q_D(const BOpenGLCamera);
    return QQuaternion::fromAxisAndAngle(d->m_upVector, angle);
}

QQuaternion BOpenGLCamera::rollRotation(const float& angle) const
{
    Q_D(const BOpenGLCamera);
    return QQuaternion::fromAxisAndAngle(d->m_cameraToCenter, -angle);
}

void BOpenGLCamera::setStandardUniforms(const QOpenGLShaderProgramPtr& program, const QMatrix4x4& model) const
{
    QMatrix4x4 modelViewMatrix = viewMatrix() * model;
    QMatrix3x3 normalMatrix = modelViewMatrix.normalMatrix();
    QMatrix4x4 mvp = viewProjectionMatrix() * model;

    program->setUniformValue("modelViewMatrix", modelViewMatrix);
    program->setUniformValue("normalMatrix", normalMatrix);
    program->setUniformValue("projectionMatrix", projectionMatrix());
    program->setUniformValue("mvp", mvp);
}

void BOpenGLCamera::setPosition(const QVector3D& position)
{
    Q_D(BOpenGLCamera);
    d->m_position = position;
    d->m_cameraToCenter = d->m_viewCenter - position;
    d->m_viewMatrixDirty = true;
}

void BOpenGLCamera::setUpVector(const QVector3D& upVector)
{
    Q_D(BOpenGLCamera);
    d->m_upVector = upVector;
    d->m_viewMatrixDirty = true;
}

void BOpenGLCamera::setViewCenter(const QVector3D& viewCenter)
{
    Q_D(BOpenGLCamera);
    d->m_viewCenter = viewCenter;
    d->m_cameraToCenter = viewCenter - d->m_position;
    d->m_viewMatrixDirty = true;
}

void BOpenGLCamera::resetToIdentity()
{
    setPosition(QVector3D(0.0f, 0.0f, 1.0f));
    setViewCenter(QVector3D(0.0f, 0.0f, 0.0f));
    setUpVector(QVector3D(0.0f, 1.0f, 0.0f));
}

void BOpenGLCamera::translate(const QVector3D& vLocal, BOpenGLCamera::CameraTranslationOption option)
{
    Q_D(BOpenGLCamera);

    // Calculate the amount to move in WC
    QVector3D vWorld;
    if (!qFuzzyIsNull(vLocal.x()))
    {
        // calculate for local x axis
        QVector3D x = QVector3D::crossProduct(d->m_cameraToCenter, d->m_upVector).normalized();
        vWorld += vLocal.x() * x;
    }
    if (!qFuzzyIsNull(vLocal.y()))
        vWorld += vLocal.y() * d->m_upVector;
    if (!qFuzzyIsNull(vLocal.z()))
        vWorld += vLocal.z() * d->m_cameraToCenter.normalized();

    // Update camera position
    d->m_position += vWorld;

    // and view center
    if (option == TranslateViewCenter)
        d->m_viewCenter += vWorld;

    // Refresh cameraToCenter
    d->m_cameraToCenter = d->m_viewCenter - d->m_position;

    // Calculate new up vector
    // 1) new local x-vector from cameraToCenter and old up
    // 2) new up vector from new local x vector (normal to up and to cameraToCenter)
    QVector3D x = QVector3D::crossProduct(d->m_cameraToCenter, d->m_upVector).normalized();
    d->m_upVector = QVector3D::crossProduct(x, d->m_cameraToCenter).normalized();

    d->m_viewMatrixDirty = true;
    d->m_viewProjectionMatrixDirty = true;
}

void BOpenGLCamera::translateWorld(const QVector3D& vWorld, BOpenGLCamera::CameraTranslationOption option)
{
    Q_D(BOpenGLCamera);

    d->m_position += vWorld;

    if (option == TranslateViewCenter)
        d->m_viewCenter += vWorld;

    d->m_cameraToCenter = d->m_viewCenter - d->m_position;

    d->m_viewMatrixDirty = true;
    d->m_viewProjectionMatrixDirty = true;
}

void BOpenGLCamera::tilt(const float& angle)
{
    QQuaternion q = tiltRotation(angle);
    rotate(q);
}

void BOpenGLCamera::pan(const float& angle)
{
    QQuaternion q = panRotation(-angle);
    rotate(q);
}

void BOpenGLCamera::roll(const float& angle)
{
    QQuaternion q = rollRotation(-angle);
    rotate(q);
}

void BOpenGLCamera::tiltAboutViewCenter(const float& angle)
{
    QQuaternion q = tiltRotation(-angle);
    rotateAboutViewCenter(q);
}

void BOpenGLCamera::panAboutViewCenter(const float& angle)
{
    QQuaternion q = panRotation(angle);
    rotateAboutViewCenter(q);
}

void BOpenGLCamera::rollAboutViewCenter(const float& angle)
{
    QQuaternion q = rollRotation(angle);
    rotateAboutViewCenter(q);
}

void BOpenGLCamera::rotate(const QQuaternion& q)
{
    Q_D(BOpenGLCamera);
    d->m_upVector = q.rotatedVector(d->m_upVector);
    d->m_cameraToCenter = q.rotatedVector(d->m_cameraToCenter);
    d->m_viewCenter = d->m_position + d->m_cameraToCenter;
    d->m_viewMatrixDirty = true;
    d->m_viewProjectionMatrixDirty = true;
}

void BOpenGLCamera::rotateAboutViewCenter(const QQuaternion& q)
{
    Q_D(BOpenGLCamera);
    d->m_upVector = q.rotatedVector(d->m_upVector);
    d->m_cameraToCenter = q.rotatedVector(d->m_cameraToCenter);
    d->m_position = d->m_viewCenter - d->m_cameraToCenter;
    d->m_viewMatrixDirty = true;
    d->m_viewProjectionMatrixDirty = true;
}

