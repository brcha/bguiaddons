/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of bguiaddons
 */

#ifndef BOPENGLCYLINDER_H
#define BOPENGLCYLINDER_H

#include <bguiaddons_export.h>

#include "bopenglabstractprimitive.h"

class BGUIADDONS_EXPORT BOpenGLCylinderPrimitive : public BOpenGLAbstractPrimitive
{
    Q_OBJECT

    Q_PROPERTY(float radius READ radius WRITE setRadius)
    Q_PROPERTY(float height READ height WRITE setHeight)
    Q_PROPERTY(int rings READ rings WRITE setRings)
    Q_PROPERTY(int slices READ slices WRITE setSlices)

public:
    explicit BOpenGLCylinderPrimitive(QObject *parent = 0);
    ~BOpenGLCylinderPrimitive();

    float radius() const;
    float height() const;
    int rings() const;
    int slices() const;

    virtual int indexCount() const;

public slots:
    void setRadius(float arg);
    void setHeight(float arg);
    void setRings(int arg);
    void setSlices(int arg);

protected:
    virtual void generateVertexData(QVector<float>& vertices
                                   ,QVector<float>& normals
                                   ,QVector<float>& texCoords
                                   ,QVector<float>& tangents
                                   ,QVector<unsigned int>& indices);

private:
    float m_radius;
    float m_height;
    int m_rings;
    int m_slices;
};

#endif // BOPENGLCYLINDER_H
