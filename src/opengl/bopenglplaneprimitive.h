/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2014, 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of bguiaddons
 */

#ifndef BOPENGLPLANE_H
#define BOPENGLPLANE_H

#include <bguiaddons_export.h>

#include "bopenglabstractprimitive.h"

class BGUIADDONS_EXPORT BOpenGLPlanePrimitive : public BOpenGLAbstractPrimitive
{
    Q_OBJECT
    Q_PROPERTY(float width READ width WRITE setWidth)
    Q_PROPERTY(float height READ height WRITE setHeight)
    Q_PROPERTY(int xDivisions READ xDivisions WRITE setXDivisions)
    Q_PROPERTY(int yDivisions READ yDivisions WRITE setYDivisions)

public:
    explicit BOpenGLPlanePrimitive(QObject *parent = 0);
    BOpenGLPlanePrimitive(float width, float height, int xDivisions, int yDivisions, QObject *parent = 0);
    ~BOpenGLPlanePrimitive();

    float width() const;
    float height() const;

    int xDivisions() const;
    int yDivisions() const;

    virtual int indexCount() const;

public slots:
    void setWidth(float arg);
    void setHeight(float arg);

    void setXDivisions(int arg);
    void setYDivisions(int arg);

protected:
    virtual void generateVertexData(QVector<float>& vertices
                                   ,QVector<float>& normals
                                   ,QVector<float>& texCoords
                                   ,QVector<float>& tangents
                                   ,QVector<unsigned int>& indices);

private:
    float m_width;
    float m_height;
    int m_xDivisions;
    int m_yDivisions;
};

#endif // BOPENGLPLANE_H
