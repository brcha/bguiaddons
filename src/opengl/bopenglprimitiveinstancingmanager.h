/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of bguiaddons
 */

#ifndef BOPENGLPRIMITIVEMANAGER_H
#define BOPENGLPRIMITIVEMANAGER_H

#include <bguiaddons_export.h>

#include <QtCore/QObject>
#include <QtCore/QVector>
#include <QtGui/QMatrix4x4>
#include <QtGui/QVector3D>

class QOpenGLBuffer;
class QOpenGLVertexArrayObject;

class QOpenGLFunctions_3_3_Core;
class QOpenGLContext;

#include "bopenglshadermanager.h"

class BOpenGLAbstractPrimitive;

class BOpenGLCamera;

class BGUIADDONS_EXPORT BOpenGLPrimitiveInstancingManager : public QObject
{
    Q_OBJECT

public:
    explicit BOpenGLPrimitiveInstancingManager(QObject *parent = 0);
    ~BOpenGLPrimitiveInstancingManager();

    BOpenGLShaderManagerPtr shaderManager() const;
    void setShaderManager(const BOpenGLShaderManagerPtr& shaderManager);

    QOpenGLContext* context() const;
    void setContext(QOpenGLContext* context);

    int addPrimitive(BOpenGLAbstractPrimitive* primitive);
    void updatePrimitive(int pidx
                        ,const QVector<QMatrix4x4>& modelMatrices
                        ,const QVector<QVector3D>& colors
                        ,const BOpenGLCamera * camera);
    void removePrimitive(int idx);

    void initialize();
    void render();

private:
    void propagateShaderManager();
    void updateVertexArrayObjects();
    void setupPrimitiveBuffers(int idx);
    void updateVertexArrayObjects(int idx);
    void bindDynamicBuffers();

private:
    QVector<BOpenGLAbstractPrimitive*> m_primitives;
    QVector<bool> m_primitiveInitialized;
    QVector<int> m_perPrimitiveCount;

    QVector<QVector<float>> m_modelViews;
    QVector<QVector<float>> m_normals;
    QVector<QVector<float>> m_materialKDs;

    QVector<QOpenGLBuffer*> m_modelViewBuffers;
    QVector<QOpenGLBuffer*> m_normalBuffers;
    QVector<QOpenGLBuffer*> m_materialKDBuffers;

    BOpenGLShaderManagerPtr m_shaderManager;

    QOpenGLFunctions_3_3_Core* m_funcs;

    QOpenGLContext* m_context;
};

#endif // BOPENGLPRIMITIVEMANAGER_H
