/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of bguiaddons
 */

#ifndef BOPENGLABSTRACTPRIMITIVE_H
#define BOPENGLABSTRACTPRIMITIVE_H

#include <bguiaddons_export.h>

#include <QtCore/QObject>
#include <QtGui/QOpenGLVertexArrayObject>
#include <QtGui/QOpenGLBuffer>

#include "bopenglshadermanager.h"

/**
 * @brief The BOpenGLAbstractPrimitive class
 *
 * This is abstract class for all OpenGL primitives.
 *
 * Basically, all you have to do is implement generateVertexData() and indexCount()
 */
class BGUIADDONS_EXPORT BOpenGLAbstractPrimitive : public QObject
{
    Q_OBJECT
public:
    explicit BOpenGLAbstractPrimitive(QObject *parent = 0);
    ~BOpenGLAbstractPrimitive();

    QOpenGLVertexArrayObject * vertexArrayObject();

    BOpenGLShaderManagerPtr shaderManager() const;
    void setShaderManager(const BOpenGLShaderManagerPtr& shaderManager);

    virtual int indexCount() const = 0;

    virtual void bindBuffers();

public slots:
    virtual void create();
    virtual void render();

protected:
    virtual void generateVertexData(QVector<float>& vertices
                                   ,QVector<float>& normals
                                   ,QVector<float>& texCoords
                                   ,QVector<float>& tangents
                                   ,QVector<unsigned int>& indices) = 0;
    void updateVertexArrayObject();

protected:
    BOpenGLShaderManagerPtr m_shaderManager;

    QOpenGLVertexArrayObject m_vao;

    QOpenGLBuffer m_positionBuffer;
    QOpenGLBuffer m_normalBuffer;
    QOpenGLBuffer m_textureCoordBuffer;
    QOpenGLBuffer m_tangentBuffer;
    QOpenGLBuffer m_indexBuffer;
};

#endif // BOPENGLABSTRACTPRIMITIVE_H
