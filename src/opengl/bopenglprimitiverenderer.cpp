/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of bguiaddons
 */

#include "bopenglprimitiverenderer.h"

#include "bopenglprimitiveinstancingmanager.h"
#include "bopenglabstractprimitive.h"
#include "bopenglcamera.h"

BOpenGLPrimitiveRenderer::BOpenGLPrimitiveRenderer(QObject *parent)
    : QObject(parent)
    , m_instancingManager(nullptr)
    , m_primitiveIndexHash()
    , m_instancedModelMatrices()
    , m_instancedColors()
    , m_singlePrimitives()
    , m_singlePrimitiveModelMatrix()
    , m_singlePrimitiveColor()
{
}

BOpenGLPrimitiveRenderer::~BOpenGLPrimitiveRenderer()
{   
}

void BOpenGLPrimitiveRenderer::setInstancingManager(BOpenGLPrimitiveInstancingManager* manager)
{
    m_instancingManager = manager;
}

void BOpenGLPrimitiveRenderer::addInstancedPrimitive(const BOpenGLPrimitiveID id
                                                    ,BOpenGLAbstractPrimitive* primitive)
{
    Q_ASSERT(m_instancingManager);
    Q_ASSERT(!m_primitiveIndexHash.contains(id));
    int idx = m_instancingManager->addPrimitive(primitive);
    m_primitiveIndexHash[id] = idx;
    m_instancedModelMatrices[id] = QVector<QMatrix4x4>();
    m_instancedColors[id] = QVector<QVector3D>();
}

void BOpenGLPrimitiveRenderer::removeInstancedPrimitive(const BOpenGLPrimitiveID id)
{
    Q_ASSERT(m_instancingManager);
    Q_ASSERT(m_primitiveIndexHash.contains(id));
    m_instancingManager->removePrimitive(m_primitiveIndexHash[id]);
    m_primitiveIndexHash.remove(id);
    m_instancedModelMatrices.remove(id);
    m_instancedColors.remove(id);
}

void BOpenGLPrimitiveRenderer::allocateInstancedPrimitive(const BOpenGLPrimitiveID id
                                                         ,const int size)
{
    Q_ASSERT(m_primitiveIndexHash.contains(id));
    m_instancedModelMatrices[id].resize(size);
    m_instancedColors[id].resize(size);
}

void BOpenGLPrimitiveRenderer::setInstancedMatrix(const BOpenGLPrimitiveID id
                                                 ,const int idx
                                                 ,const QMatrix4x4& matrix)
{
    Q_ASSERT(m_primitiveIndexHash.contains(id));
    m_instancedModelMatrices[id][idx] = matrix;
}

void BOpenGLPrimitiveRenderer::setInstancedColor(const BOpenGLPrimitiveID id
                                                ,const int idx
                                                ,const QColor& color)
{
    Q_ASSERT(m_primitiveIndexHash.contains(id));
    m_instancedColors[id][idx] = QVector3D(color.redF(), color.greenF(), color.blueF());
}

void BOpenGLPrimitiveRenderer::setInstancedColor(const BOpenGLPrimitiveID id
                                                ,const int idx
                                                ,const QVector3D& color)
{
    Q_ASSERT(m_primitiveIndexHash.contains(id));
    m_instancedColors[id][idx] = color;
}

void BOpenGLPrimitiveRenderer::setInstancedMatrices(const BOpenGLPrimitiveID id
                                                   ,const QVector<QMatrix4x4>& matrices)
{
    Q_ASSERT(m_primitiveIndexHash.contains(id));
    Q_ASSERT(m_instancedModelMatrices[id].size() == matrices.size());
    m_instancedModelMatrices[id] = matrices;
}

void BOpenGLPrimitiveRenderer::setInstancedColors(const BOpenGLPrimitiveID id
                                                 ,const QVector<QColor>& colors)
{
    Q_ASSERT(m_primitiveIndexHash.contains(id));
    Q_ASSERT(m_instancedColors[id].size() == colors.size());
    for (int i=0; i < colors.size(); ++i)
    {
        setInstancedColor(id, i, colors[i]);
    }
}

void BOpenGLPrimitiveRenderer::setInstancedColors(const BOpenGLPrimitiveID id
                                                 ,const QVector<QVector3D>& colors)
{
    Q_ASSERT(m_primitiveIndexHash.contains(id));
    Q_ASSERT(m_instancedColors[id].size() == colors.size());
    m_instancedColors[id] = colors;
}

void BOpenGLPrimitiveRenderer::addSinglePrimitive(const BOpenGLPrimitiveID id
                                                 ,BOpenGLAbstractPrimitive* primitive)
{
    Q_ASSERT(!m_singlePrimitives.contains(id));
    m_singlePrimitives[id] = primitive;
    m_singlePrimitiveModelMatrix[id] = QMatrix4x4();
    m_singlePrimitiveColor[id] = QVector3D();
}

void BOpenGLPrimitiveRenderer::removeSinglePrimitive(const BOpenGLPrimitiveID id)
{
    Q_ASSERT(m_singlePrimitives.contains(id));
    m_singlePrimitives.remove(id);
    m_singlePrimitiveModelMatrix.remove(id);
    m_singlePrimitiveColor.remove(id);
}

void BOpenGLPrimitiveRenderer::setSingleMatrix(const BOpenGLPrimitiveID id
                                              ,const QMatrix4x4& matrix)
{
    Q_ASSERT(m_singlePrimitives.contains(id));
    m_singlePrimitiveModelMatrix[id] = matrix;
}

void BOpenGLPrimitiveRenderer::setSingleColor(const BOpenGLPrimitiveID id
                                             ,const QColor& color)
{
    Q_ASSERT(m_singlePrimitives.contains(id));
    m_singlePrimitiveColor[id] = QVector3D(color.redF(), color.greenF(), color.blueF());;
}

void BOpenGLPrimitiveRenderer::setSingleColor(const BOpenGLPrimitiveID id
                                             ,const QVector3D& color)
{
    Q_ASSERT(m_singlePrimitives.contains(id));
    m_singlePrimitiveColor[id] = color;
}

void BOpenGLPrimitiveRenderer::render(const BOpenGLCamera* camera)
{
    Q_ASSERT(m_instancingManager);

    // First send data to instancing manager
    foreach (auto id, m_primitiveIndexHash.keys()) {
        m_instancingManager->updatePrimitive(m_primitiveIndexHash[id]
                                            ,m_instancedModelMatrices[id]
                                            ,m_instancedColors[id]
                                            ,camera);
    }

    // Render instanced stuff
    m_instancingManager->shaderManager()->bind();
    m_instancingManager->render();

    // Render our primitives
    foreach (auto id, m_singlePrimitives.keys()) {
        m_singlePrimitives[id]->shaderManager()->bind();
        QOpenGLShaderProgramPtr shader = m_singlePrimitives[id]->shaderManager()->shader();
        shader->setUniformValue("material.kd", m_singlePrimitiveColor[id]);
        camera->setStandardUniforms(shader, m_singlePrimitiveModelMatrix[id]);
        m_singlePrimitives[id]->render();
    }
}

BOpenGLShaderManagerPtr BOpenGLPrimitiveRenderer::instancingShader() const
{
    Q_ASSERT(m_instancingManager);
    return m_instancingManager->shaderManager();
}

BOpenGLShaderManagerPtr BOpenGLPrimitiveRenderer::singleShader(BOpenGLPrimitiveID id) const
{
    Q_ASSERT(m_singlePrimitives.contains(id));
    return m_singlePrimitives[id]->shaderManager();
}

