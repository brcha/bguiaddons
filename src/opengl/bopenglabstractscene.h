/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2014, 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of bguiaddons
 */

#ifndef BOPENGLABSTRACTSCENE_H
#define BOPENGLABSTRACTSCENE_H

#include <bguiaddons_export.h>

#include <QtCore/QObject>

class QOpenGLContext;

class BGUIADDONS_EXPORT BOpenGLAbstractScene : public QObject
{
    Q_OBJECT
public:
    explicit BOpenGLAbstractScene(QObject *parent = 0);
    virtual ~BOpenGLAbstractScene();

    QOpenGLContext* context() const;
    void setContext(QOpenGLContext* context);

    // Initialize the scene
    virtual void initialize() = 0;

    // Clean up the scene
    virtual void cleanup();

    // Update the scene
    virtual void update(float t) = 0;

    // Render the scene
    virtual void render() = 0;

    // Resize the scene
    virtual void resize(int w, int h) = 0;

protected:
    QOpenGLContext * m_context;
};

#endif // BOPENGLABSTRACTSCENE_H
