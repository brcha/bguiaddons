/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2014, 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of bguiaddons
 */

#include "bopenglsampler.h"

#include <QtGui/QOpenGLContext>
#include <QtGui/QOpenGLFunctions_3_3_Core>
#include <QtOpenGLExtensions/QOpenGLExtensions>

class BOpenGLSamplerPrivate
{
public:
    BOpenGLSamplerPrivate(BOpenGLSampler * qq)
        : q_ptr(qq)
#if !defined(QT_OPENGL_ES_2)
        , m_samplerId(0)
#endif
    {}

    bool create(QOpenGLContext * ctx);
    void destroy();
    void bind(GLuint unit);
    void release(GLuint unit);
    void setParameter(GLenum param, GLenum value);
    void setParameter(GLenum param, float value);

    Q_DECLARE_PUBLIC(BOpenGLSampler)

    BOpenGLSampler * q_ptr;

#if !defined(QT_OPENGL_ES_2)
    GLuint m_samplerId;

    union {
        QOpenGLFunctions_3_3_Core * core;
        QOpenGLExtension_ARB_sampler_objects * arb;
    } m_funcs;

    enum {
        NotSupported,
        Core,
        ARB
    } m_funcsType;
#endif /* !defined(QT_OPENGL_ES_2) */
};

bool BOpenGLSamplerPrivate::create(QOpenGLContext *ctx)
{
#if defined(QT_OPENGL_ES_2)
    qFatal("Sampler is not supported on OpenGL ES 2");
    return false;
#endif
    m_funcs.core = nullptr;
    m_funcsType = NotSupported;

    QSurfaceFormat format = ctx->format();
    const int version = (format.majorVersion() << 8) + format.minorVersion();

    if (version >= 0x0303)
    {
        m_funcs.core = ctx->versionFunctions<QOpenGLFunctions_3_3_Core>();
        if (!m_funcs.core)
            return false;
        m_funcsType = Core;
        m_funcs.core->initializeOpenGLFunctions();
        m_funcs.core->glGenSamplers(1, &m_samplerId);
    }
    else if (ctx->hasExtension("GL_ARB_sampler_objects"))
    {
        m_funcs.arb = new QOpenGLExtension_ARB_sampler_objects;
        if (!m_funcs.arb->initializeOpenGLFunctions())
            return false;
        m_funcsType = ARB;
        m_funcs.arb->glGenSamplers(1, &m_samplerId);
    }

    return (m_samplerId != 0);
}

void BOpenGLSamplerPrivate::destroy()
{
#if defined(QT_OPENGL_ES_2)
    qFatal("Sampler is not supported on OpenGL ES 2");
    return;
#endif
    switch (m_funcsType) {
        case Core:
            m_funcs.core->glDeleteSamplers(1, &m_samplerId);
            break;
        case ARB:
            m_funcs.arb->glDeleteSamplers(1, &m_samplerId);
            break;
        case NotSupported:
        default:
            qt_noop();
            break;
    }
}

void BOpenGLSamplerPrivate::bind(GLuint unit)
{
#if defined(QT_OPENGL_ES_2)
    qFatal("Sampler is not supported on OpenGL ES 2");
    return;
#endif
    switch (m_funcsType) {
        case Core:
            m_funcs.core->glBindSampler(unit, m_samplerId);
            break;
        case ARB:
            m_funcs.arb->glBindSampler(unit, m_samplerId);
            break;
        case NotSupported:
        default:
            qt_noop();
            break;
    }
}

void BOpenGLSamplerPrivate::release(GLuint unit)
{
#if defined(QT_OPENGL_ES_2)
    qFatal("Sampler is not supported on OpenGL ES 2");
    return;
#endif
    switch (m_funcsType) {
        case Core:
            m_funcs.core->glBindSampler(unit, 0);
            break;
        case ARB:
            m_funcs.arb->glBindSampler(unit, 0);
            break;
        case NotSupported:
        default:
            qt_noop();
            break;
    }
}

void BOpenGLSamplerPrivate::setParameter(GLenum param, GLenum value)
{
#if defined(QT_OPENGL_ES_2)
    qFatal("Sampler is not supported on OpenGL ES 2");
    return;
#endif
    switch (m_funcsType) {
        case Core:
            m_funcs.core->glSamplerParameteri(m_samplerId, param, value);
            break;
        case ARB:
            m_funcs.arb->glSamplerParameteri(m_samplerId, param, value);
            break;
        case NotSupported:
        default:
            qt_noop();
            break;
    }
}

void BOpenGLSamplerPrivate::setParameter(GLenum param, float value)
{
#if defined(QT_OPENGL_ES_2)
    qFatal("Sampler is not supported on OpenGL ES 2");
    return;
#endif
    switch (m_funcsType) {
        case Core:
            m_funcs.core->glSamplerParameterf(m_samplerId, param, value);
            break;
        case ARB:
            m_funcs.arb->glSamplerParameterf(m_samplerId, param, value);
            break;
        case NotSupported:
        default:
            qt_noop();
            break;
    }
}

BOpenGLSampler::BOpenGLSampler()
    : d_ptr(new BOpenGLSamplerPrivate(this))
{
}

BOpenGLSampler::~BOpenGLSampler()
{
}

bool BOpenGLSampler::create()
{
    QOpenGLContext * context = QOpenGLContext::currentContext();
    Q_ASSERT(context);
    Q_D(BOpenGLSampler);
    return d->create(context);
}

void BOpenGLSampler::destroy()
{
    Q_D(BOpenGLSampler);
    d->destroy();
}

bool BOpenGLSampler::isCreated() const
{
    Q_D(const BOpenGLSampler);
    return (d->m_samplerId != 0);
}

GLuint BOpenGLSampler::samplerId() const
{
    Q_D(const BOpenGLSampler);
    return d->m_samplerId;
}

void BOpenGLSampler::bind(GLuint unit)
{
    Q_D(BOpenGLSampler);
    d->bind(unit);
}

void BOpenGLSampler::release(GLuint unit)
{
    Q_D(BOpenGLSampler);
    d->release(unit);
}

void BOpenGLSampler::setWrapMode(CoordinateDirection direction, GLenum wrapMode)
{
    Q_D(BOpenGLSampler);
    d->setParameter(direction, wrapMode);
}

void BOpenGLSampler::setMinificationFilter(GLenum filter)
{
    Q_D(BOpenGLSampler);
    d->setParameter(GL_TEXTURE_MIN_FILTER, filter);
}

void BOpenGLSampler::setMagnificationFilter(GLenum filter)
{
    Q_D(BOpenGLSampler);
    d->setParameter(GL_TEXTURE_MAG_FILTER, filter);
}

void BOpenGLSampler::setMaximumAnisotropy(float anisotropy)
{
    Q_D(BOpenGLSampler);
    d->setParameter(GL_TEXTURE_MAX_ANISOTROPY_EXT, anisotropy);
}
