/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of bguiaddons
 */

#include "bopenglsphereprimitive.h"

#include <QtGui/QOpenGLShaderProgram>

#include <math.h>

const float pi = 3.14159265358979323846;
const float twoPi = 2.0 * pi;

BOpenGLSpherePrimitive::BOpenGLSpherePrimitive(QObject *parent)
    : BOpenGLAbstractPrimitive(parent)
    , m_radius(1.0f)
    , m_rings(30)
    , m_slices(30)
    , m_normalLines(QOpenGLBuffer::VertexBuffer)
    , m_tangentLines(QOpenGLBuffer::VertexBuffer)
    , m_vaoNormals()
    , m_vaoTangents()
{
}

BOpenGLSpherePrimitive::~BOpenGLSpherePrimitive()
{

}

float BOpenGLSpherePrimitive::radius() const
{
    return m_radius;
}

int BOpenGLSpherePrimitive::rings() const
{
    return m_rings;
}

int BOpenGLSpherePrimitive::slices() const
{
    return m_slices;
}

void BOpenGLSpherePrimitive::setRadius(float arg)
{
    m_radius = arg;
}

void BOpenGLSpherePrimitive::setRings(int arg)
{
    m_rings = arg;
}

void BOpenGLSpherePrimitive::setSlices(int arg)
{
    m_slices = arg;
}

void BOpenGLSpherePrimitive::generateVertexData(QVector<float>& vertices, QVector<float>& normals
                               ,QVector<float>& texCoords, QVector<float>& tangents
                               ,QVector<unsigned int>& indices)
{
    int faces = (m_slices - 2) * m_rings // number of "rectangular" faces
              + (m_rings * 2); // and rings for top and bottom caps
    int nVerts = (m_slices + 1) * (m_rings + 1);

    // Resize data buffers
    vertices.resize(3 * nVerts);
    normals.resize(3 * nVerts);
    texCoords.resize(2 * nVerts);
    tangents.resize(4 * nVerts);
    indices.resize(6 * faces);

    const float dTheta = twoPi / static_cast<float>(m_slices);
    const float dPhi = pi / static_cast<float>(m_rings);
    const float du = 1.0f / static_cast<float>(m_slices);
    const float dv = 1.0f / static_cast<float>(m_rings);

    // Iterate latitudes, ie. rings
    int index = 0;
    int texCoordIndex = 0;
    int tangentIndex = 0;
    for (int lat = 0; lat < m_rings + 1; ++lat)
    {
        const float phi = pi / 2.0f - static_cast<float>(lat) * dPhi;
        const float cosPhi = cosf(phi);
        const float sinPhi = sinf(phi);
        const float v = 1.0f - static_cast<float>(lat) * dv;

        // Iterate longitudes, ie. slices
        for (int lon = 0; lon < m_slices + 1; ++lon)
        {
            const float theta = static_cast<float>(lon) * dTheta;
            const float cosTheta = cosf(theta);
            const float sinTheta = sinf(theta);
            const float u = static_cast<float>(lon) * du;

            vertices[index]   = m_radius * cosTheta * cosPhi;
            vertices[index+1] = m_radius * sinPhi;
            vertices[index+2] = m_radius * sinTheta * cosPhi;

            normals[index]   = cosTheta * cosPhi;
            normals[index+1] = sinPhi;
            normals[index+2] = sinTheta * cosPhi;

            tangents[tangentIndex]   = sinTheta;
            tangents[tangentIndex+1] = 0.0f;
            tangents[tangentIndex+2] = -cosTheta;
            tangents[tangentIndex+3] = 1.0f;

            tangentIndex += 4;
            index += 3;

            texCoords[texCoordIndex]   = u;
            texCoords[texCoordIndex+1] = v;

            texCoordIndex += 2;
        }
    }

    int elIndex = 0;

    // top cap
    {
        const int nextRingStartIndex = m_slices + 1;
        for (int j = 0; j < m_slices; ++j)
        {
            indices[elIndex]   = nextRingStartIndex + j;
            indices[elIndex+1] = 0;
            indices[elIndex+2] = nextRingStartIndex + j + 1;

            elIndex += 3;
        }
    }

    for (int i=1; i<(m_rings - 1); ++i)
    {
        const int ringStartIndex = i * (m_slices + 1);
        const int nextRingStartIndex = (i+1) * (m_slices+1);

        for (int j=0; j < m_slices; ++j)
        {
            // split quads into triangles
            indices[elIndex]   = ringStartIndex + j;
            indices[elIndex+1] = ringStartIndex + j + 1;
            indices[elIndex+2] = nextRingStartIndex + j;
            indices[elIndex+3] = nextRingStartIndex + j;
            indices[elIndex+4] = ringStartIndex + j + 1;
            indices[elIndex+5] = nextRingStartIndex + j + 1;

            elIndex += 6;
        }
    }

    // bottom cap
    {
        const int ringStartIndex = (m_rings - 1) * (m_slices + 1);
        const int nextRingStartIndex = (m_rings) * (m_slices + 1);

        for (int j=0; j < m_slices; ++j)
        {
            indices[elIndex]   = ringStartIndex + j;
            indices[elIndex+1] = nextRingStartIndex;
            indices[elIndex+2] = ringStartIndex + j + 1;

            elIndex += 3;
        }
    }
}


int BOpenGLSpherePrimitive::indexCount() const
{
    return 6 * m_slices * m_rings;
}

void BOpenGLSpherePrimitive::computeNormalLinesBuffer(const BOpenGLShaderManagerPtr& mat, double scale)
{
    int nVerts = (m_slices + 1) * (m_rings + 1);
    float * v = new float[6 * nVerts];
    float * vPtr = v;

    m_positionBuffer.bind();
    float * p = reinterpret_cast<float*> (m_positionBuffer.map(QOpenGLBuffer::ReadOnly));
    Q_ASSERT(p);

    m_normalBuffer.bind();
    float * n = reinterpret_cast<float*> (m_normalBuffer.map(QOpenGLBuffer::ReadOnly));
    Q_ASSERT(n);

    for (int vIndex = 0; vIndex < nVerts; ++vIndex)
    {
        float x = *p++;
        float y = *p++;
        float z = *p++;

        // point on the sphere
        *vPtr++ = x;
        *vPtr++ = y;
        *vPtr++ = z;
        // point on the normal of the sphere, of the length "scale"
        *vPtr++ = x + (*n++ * scale);
        *vPtr++ = y + (*n++ * scale);
        *vPtr++ = z + (*n++ * scale);
    }

    m_normalBuffer.unmap();
    m_positionBuffer.bind();
    m_positionBuffer.unmap();

    // Create buffer
    m_normalLines.create();
    m_normalLines.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_normalLines.bind();
    m_normalLines.allocate(v, 6*nVerts * sizeof(float));

    m_vaoNormals.create();
    m_vaoNormals.bind();
    mat->shader()->enableAttributeArray("vertexPosition");
    mat->shader()->setAttributeArray("vertexPosition", GL_FLOAT, 0, 3);

    m_vaoNormals.release();
    m_normalLines.release();

    delete [] v;
}

void BOpenGLSpherePrimitive::computeTangentLinesBuffer(const BOpenGLShaderManagerPtr& mat, double scale)
{
    int nVerts = (m_slices+1) * (m_rings+1);
    float * v = new float[6 * nVerts];
    float * vPtr = v;

    m_tangentBuffer.bind();
    float * t = reinterpret_cast<float*> (m_tangentBuffer.map(QOpenGLBuffer::ReadOnly));
    Q_ASSERT(t);

    m_positionBuffer.bind();
    float * p = reinterpret_cast<float*> (m_positionBuffer.map(QOpenGLBuffer::ReadOnly));
    Q_ASSERT(p);

    for (int vIndex = 0; vIndex < nVerts; ++vIndex)
    {
        float x = *p++;
        float y = *p++;
        float z = *p++;

        // point on sphere
        *vPtr++ = x;
        *vPtr++ = y;
        *vPtr++ = z;
        // point on the tangent, 'scale' away from sphere
        *vPtr++ = x + (*t++ * scale);
        *vPtr++ = y + (*t++ * scale);
        *vPtr++ = z + (*t++ * scale);
        t++; // skip 4th tangent value
    }

    m_positionBuffer.unmap();
    m_tangentBuffer.bind();
    m_tangentBuffer.unmap();

    m_tangentLines.create();
    m_tangentLines.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_tangentLines.bind();
    m_tangentLines.allocate(v, 6 * nVerts * sizeof(float));

    m_vaoTangents.create();
    m_vaoTangents.bind();
    mat->shader()->enableAttributeArray("vertexPosition");
    mat->shader()->setAttributeArray("vertexPosition", GL_FLOAT, 0, 3);

    m_vaoTangents.release();
    m_tangentLines.release();

    delete [] v;
}

void BOpenGLSpherePrimitive::renderNormalLines()
{
    int nVerts = (m_slices+1) * (m_rings+1);

    m_vaoNormals.bind();

    glDrawArrays(GL_LINES, 0, nVerts*2);

    m_vaoNormals.release();
}

void BOpenGLSpherePrimitive::renderTangentLines()
{
    int nVerts = (m_slices+1) * (m_rings+1);

    m_vaoTangents.bind();

    glDrawArrays(GL_LINES, 0, nVerts*2);

    m_vaoTangents.release();
}

void BOpenGLSpherePrimitive::bindBuffers()
{
    QOpenGLShaderProgramPtr shader = m_shaderManager->shader();

    shader->bind();

    m_positionBuffer.bind();
    shader->enableAttributeArray("vertexPosition");
    shader->setAttributeArray("vertexPosition", GL_FLOAT, 0, 3);

    m_normalBuffer.bind();
    shader->enableAttributeArray("vertexNormal");
    shader->setAttributeArray("vertexNormal", GL_FLOAT, 0, 3);

    m_textureCoordBuffer.bind();
    shader->enableAttributeArray("vertexTexCoord");
    shader->setAttributeArray("vertexTexCoord", GL_FLOAT, 0, 2);

    m_tangentBuffer.bind();
    shader->enableAttributeArray("vertexTangent");
    shader->setAttributeArray("vertexTangent", GL_FLOAT, 0, 4);

    m_indexBuffer.bind();
}


