/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2014, 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of bguiaddons
 */

#ifndef BOPENGLSAMPLER_H
#define BOPENGLSAMPLER_H

#include <bguiaddons_export.h>

#include <QtGui/qopengl.h>
#include <QtCore/QSharedPointer>

class QOpenGLFunctions_3_3_Core;

class BOpenGLSamplerPrivate;

class BGUIADDONS_EXPORT BOpenGLSampler
{
public:
    BOpenGLSampler();
    ~BOpenGLSampler();

    bool create();
    void destroy();
    bool isCreated() const;
    GLuint samplerId() const;
    void bind(GLuint unit);
    void release(GLuint unit);

    enum CoordinateDirection {
        DirectionS = GL_TEXTURE_WRAP_S,
        DirectionT = GL_TEXTURE_WRAP_T,
        DirectionR = GL_TEXTURE_WRAP_R
    };

    void setWrapMode(CoordinateDirection direction, GLenum wrapMode);
    void setMinificationFilter(GLenum filter);
    void setMagnificationFilter(GLenum filter);
    void setMaximumAnisotropy(float anisotropy);

private:
    Q_DECLARE_PRIVATE(BOpenGLSampler)

    BOpenGLSamplerPrivate * d_ptr;
};

typedef QSharedPointer<BOpenGLSampler> BOpenGLSamplerPtr;

#endif // BOPENGLSAMPLER_H
