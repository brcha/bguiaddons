/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of bguiaddons
 */

#include "bopenglcylinderprimitive.h"

#include <QtGui/QOpenGLShaderProgram>

#include <math.h>

const float pi = 3.14159265358979323846;
const float twoPi = 2.0 * pi;

BOpenGLCylinderPrimitive::BOpenGLCylinderPrimitive(QObject *parent)
    : BOpenGLAbstractPrimitive(parent)
    , m_radius(1.0f)
    , m_height(1.0f)
    , m_rings(30)
    , m_slices(30)
{
}

BOpenGLCylinderPrimitive::~BOpenGLCylinderPrimitive()
{
}

float BOpenGLCylinderPrimitive::radius() const
{
    return m_radius;
}

float BOpenGLCylinderPrimitive::height() const
{
    return m_height;
}

int BOpenGLCylinderPrimitive::rings() const
{
    return m_rings;
}

int BOpenGLCylinderPrimitive::slices() const
{
    return m_slices;
}

void BOpenGLCylinderPrimitive::setRadius(float arg)
{
    m_radius = arg;
}

void BOpenGLCylinderPrimitive::setHeight(float arg)
{
    m_height = arg;
}

void BOpenGLCylinderPrimitive::setRings(int arg)
{
    m_rings = arg;
}

void BOpenGLCylinderPrimitive::setSlices(int arg)
{
    m_slices = arg;
}

void BOpenGLCylinderPrimitive::generateVertexData(QVector<float>& vertices, QVector<float>& normals
                                 ,QVector<float>& texCoords, QVector<float>& tangents
                                 ,QVector<unsigned int>& indices)
{
    int faces = m_slices * m_rings; // number of rectangles
    int nVerts = (m_slices + 1) * (m_rings + 1);

    // resize data buffers
    vertices.resize(3 * nVerts);
    normals.resize(3 * nVerts);
    texCoords.resize(2 * nVerts);
    tangents.resize(3 * nVerts);
    indices.resize(6 * faces);

    const float dTheta = twoPi / static_cast<float>(m_slices);
    const float dh = m_height / static_cast<float>(m_rings);
    const float du = 1.0f / static_cast<float>(m_slices);
    const float dv = 1.0f / static_cast<float>(m_rings);
    const float h2 = m_height / 2.0f;

    // Iterate height, ie. rings
    int index = 0;
    int texCoordIndex = 0;
    for (int i = 0; i <= m_rings; ++i)
    {
        const float h = h2 - static_cast<float>(i) * dh;
        const float v = 1.0f - static_cast<float>(i) * dv;

        // Iterate angle, ie. slices
        for (int j = 0; j <= m_slices; ++j)
        {
            const float theta = static_cast<float>(j) * dTheta;
            const float cosTheta = cosf(theta);
            const float sinTheta = sinf(theta);
            const float u = static_cast<float>(j) * du;

            vertices[index]   = m_radius * cosTheta;
            vertices[index+1] = h;
            vertices[index+2] = m_radius * sinTheta;

            normals[index]   = cosTheta;
            normals[index+1] = 0.0f;
            normals[index+2] = sinTheta;

            tangents[index]   = sinTheta;
            tangents[index+1] = 0.0f;
            tangents[index+2] = -cosTheta;

            index += 3;

            texCoords[texCoordIndex]   = u;
            texCoords[texCoordIndex+1] = v;

            texCoordIndex += 2;
        }
    }

    int elIndex = 0;

    for (int i=0; i < m_rings; ++i)
    {
        const int ringStartIndex = i * (m_slices + 1);
        const int nextRingStartIndex = (i+1) * (m_slices+1);

        for (int j=0; j < m_slices; ++j)
        {
            // quads -> two triangles
            indices[elIndex]   = ringStartIndex + j;
            indices[elIndex+1] = ringStartIndex + j + 1;
            indices[elIndex+2] = nextRingStartIndex + j;
            indices[elIndex+3] = nextRingStartIndex + j;
            indices[elIndex+4] = ringStartIndex + j + 1;
            indices[elIndex+5] = nextRingStartIndex + j + 1;

            elIndex += 6;
        }
    }
}

int BOpenGLCylinderPrimitive::indexCount() const
{
    return 6 * m_slices * m_rings;
}
