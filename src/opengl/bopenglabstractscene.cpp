/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2014, 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of bguiaddons
 */

#include "bopenglabstractscene.h"

BOpenGLAbstractScene::BOpenGLAbstractScene(QObject *parent) : QObject(parent)
{

}

BOpenGLAbstractScene::~BOpenGLAbstractScene()
{

}
QOpenGLContext* BOpenGLAbstractScene::context() const
{
    return m_context;
}

void BOpenGLAbstractScene::setContext(QOpenGLContext* context)
{
    m_context = context;
}

void BOpenGLAbstractScene::cleanup()
{
}


