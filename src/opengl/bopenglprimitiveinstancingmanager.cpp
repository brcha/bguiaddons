/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of bguiaddons
 */

#include "bopenglprimitiveinstancingmanager.h"

#include <QtGui/QOpenGLVertexArrayObject>
#include <QtGui/QOpenGLShaderProgram>
#include <QtGui/QOpenGLFunctions_3_3_Core>
#include <QtGui/QMatrix3x3>

#include "bopenglabstractprimitive.h"

#include "bopenglcamera.h"

BOpenGLPrimitiveInstancingManager::BOpenGLPrimitiveInstancingManager(QObject *parent)
    : QObject(parent)
    , m_primitives()
    , m_perPrimitiveCount()
    , m_modelViews()
    , m_normals()
    , m_materialKDs()
    , m_modelViewBuffers()
    , m_normalBuffers()
    , m_materialKDBuffers()
    , m_shaderManager()
    , m_funcs(nullptr)
{
}

BOpenGLPrimitiveInstancingManager::~BOpenGLPrimitiveInstancingManager()
{
    foreach (QOpenGLBuffer* b, m_modelViewBuffers) {
        delete b;
    }

    foreach (QOpenGLBuffer* b, m_normalBuffers) {
        delete b;
    }

    foreach (QOpenGLBuffer* b, m_materialKDBuffers) {
        delete b;
    }
}

void BOpenGLPrimitiveInstancingManager::initialize()
{
    m_funcs = m_context->versionFunctions<QOpenGLFunctions_3_3_Core>();
    if (!m_funcs)
        qFatal("Couldn't initialize OpenGL functions!");

    m_funcs->initializeOpenGLFunctions();
}

void BOpenGLPrimitiveInstancingManager::render()
{
    bindDynamicBuffers();

    for (int i = 0; i < m_primitives.size(); ++i)
    {
        m_primitives[i]->vertexArrayObject()->bind();
        m_funcs->glDrawElementsInstanced(GL_TRIANGLES
                                        ,m_primitives[i]->indexCount()
                                        ,GL_UNSIGNED_INT
                                        ,0
                                        ,m_perPrimitiveCount[i]);
        m_primitives[i]->vertexArrayObject()->release();
    }
}

BOpenGLShaderManagerPtr BOpenGLPrimitiveInstancingManager::shaderManager() const
{
    return m_shaderManager;
}

void BOpenGLPrimitiveInstancingManager::setShaderManager(const BOpenGLShaderManagerPtr& shaderManager)
{
    if (shaderManager == m_shaderManager)
        return;
    m_shaderManager = shaderManager;
    propagateShaderManager();
    updateVertexArrayObjects();
}

void BOpenGLPrimitiveInstancingManager::propagateShaderManager()
{
    foreach (BOpenGLAbstractPrimitive* prim, m_primitives) {
        prim->setShaderManager(m_shaderManager);
    }
}

void BOpenGLPrimitiveInstancingManager::updateVertexArrayObjects()
{
    QOpenGLShaderProgramPtr shader = m_shaderManager->shader();
    shader->bind();

    GLuint mvLocation = shader->attributeLocation("modelViewMatrix");
    GLuint normLocation = shader->attributeLocation("normalMatrix");
    GLuint matLocation = shader->attributeLocation("materialKD");

    for (int idx = 0; idx < m_primitives.size(); ++idx)
    {
        m_primitives[idx]->vertexArrayObject()->bind();

        m_modelViewBuffers[idx]->bind();
        for (GLuint i = 0; i < 4; ++i)
        {
            shader->enableAttributeArray(mvLocation+i);
            shader->setAttributeArray(mvLocation+i, GL_FLOAT
                                     ,(void*)(sizeof(float) * 4 * i)
                                     ,4
                                     ,sizeof(float) * 16);
            m_funcs->glVertexAttribDivisor(mvLocation+i, 1);
        }

        m_normalBuffers[idx]->bind();
        for (GLuint i = 0; i < 3; ++i)
        {
            shader->enableAttributeArray(normLocation+i);
            shader->setAttributeArray(normLocation+i, GL_FLOAT
                                     ,(void*)(sizeof(float) * 3 * i)
                                     ,3
                                     ,sizeof(float) * 9);
            m_funcs->glVertexAttribDivisor(normLocation+i, 1);
        }

        m_materialKDBuffers[idx]->bind();
        shader->enableAttributeArray(matLocation);
        shader->setAttributeArray(matLocation, GL_FLOAT, 0, 3);
        m_funcs->glVertexAttribDivisor(matLocation, 1);

        m_primitives[idx]->vertexArrayObject()->release();
    }
}

void BOpenGLPrimitiveInstancingManager::setupPrimitiveBuffers(int idx)
{
    QOpenGLBuffer * mv = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    mv->create();
    mv->setUsagePattern(QOpenGLBuffer::DynamicDraw);
    mv->bind();
    mv->allocate(m_modelViews[idx].data(), m_modelViews[idx].size() * sizeof(float));
    QOpenGLBuffer * temp = m_modelViewBuffers[idx];
    m_modelViewBuffers[idx] = mv;
    delete temp;

    QOpenGLBuffer * n = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    n->create();
    n->setUsagePattern(QOpenGLBuffer::DynamicDraw);
    n->bind();
    n->allocate(m_normals[idx].data(), m_normals[idx].size() * sizeof(float));
    temp = m_normalBuffers[idx];
    m_normalBuffers[idx] = n;
    delete temp;

    QOpenGLBuffer * m = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    m->create();
    m->setUsagePattern(QOpenGLBuffer::DynamicDraw);
    m->bind();
    m->allocate(m_materialKDs[idx].data(), m_materialKDs[idx].size() * sizeof(float));
    temp = m_materialKDBuffers[idx];
    m_materialKDBuffers[idx] = m;
    delete temp;

    updateVertexArrayObjects(idx);
}

void BOpenGLPrimitiveInstancingManager::updateVertexArrayObjects(int idx)
{
    QOpenGLShaderProgramPtr shader = m_shaderManager->shader();
    shader->bind();

    GLuint mvLocation = shader->attributeLocation("modelViewMatrix");
    GLuint normLocation = shader->attributeLocation("normalMatrix");
    GLuint matLocation = shader->attributeLocation("materialKD");

    m_primitives[idx]->vertexArrayObject()->bind();

    m_modelViewBuffers[idx]->bind();
    for (GLuint i = 0; i < 4; ++i)
    {
        shader->enableAttributeArray(mvLocation+i);
        shader->setAttributeArray(mvLocation+i, GL_FLOAT
                                 ,(void*)(sizeof(float) * 4 * i)
                                 ,4
                                 ,sizeof(float) * 16);
        m_funcs->glVertexAttribDivisor(mvLocation+i, 1);
    }

    m_normalBuffers[idx]->bind();
    for (GLuint i = 0; i < 3; ++i)
    {
        shader->enableAttributeArray(normLocation+i);
        shader->setAttributeArray(normLocation+i, GL_FLOAT
                                 ,(void*)(sizeof(float) * 3 * i)
                                 ,3
                                 ,sizeof(float) * 9);
        m_funcs->glVertexAttribDivisor(normLocation+i, 1);
    }

    m_materialKDBuffers[idx]->bind();
    shader->enableAttributeArray(matLocation);
    shader->setAttributeArray(matLocation, GL_FLOAT, 0, 3);
    m_funcs->glVertexAttribDivisor(matLocation, 1);

    m_primitives[idx]->vertexArrayObject()->release();
}

void BOpenGLPrimitiveInstancingManager::bindDynamicBuffers()
{
    for (int i = 0; i < m_modelViewBuffers.size(); ++i)
    {
        m_modelViewBuffers[i]->bind();
        m_modelViewBuffers[i]->allocate(m_modelViews[i].data()
                                       ,m_modelViews[i].size() * sizeof(float));
        m_normalBuffers[i]->bind();
        m_normalBuffers[i]->allocate(m_normals[i].data()
                                    ,m_normals[i].size() * sizeof(float));
        m_materialKDBuffers[i]->bind();
        m_materialKDBuffers[i]->allocate(m_materialKDs[i].data()
                                        ,m_materialKDs[i].size() * sizeof(float));
    }
}

QOpenGLContext* BOpenGLPrimitiveInstancingManager::context() const
{
    return m_context;
}

void BOpenGLPrimitiveInstancingManager::setContext(QOpenGLContext* context)
{
    m_context = context;
}

int BOpenGLPrimitiveInstancingManager::addPrimitive(BOpenGLAbstractPrimitive* primitive)
{
    int idx = m_primitives.size();
    m_primitives << primitive;
    m_perPrimitiveCount << 0;
    m_modelViews << QVector<float>();
    m_normals << QVector<float>();
    m_materialKDs << QVector<float>();

    // add temp buffers
    m_modelViewBuffers << new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    m_normalBuffers << new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    m_materialKDBuffers << new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);

    m_primitiveInitialized << false;

    return idx;
}

void BOpenGLPrimitiveInstancingManager::updatePrimitive(int pidx
                                                       ,const QVector<QMatrix4x4>& modelMatrices
                                                       ,const QVector<QVector3D>& colors
                                                       ,const BOpenGLCamera* camera)
{
    Q_ASSERT(pidx < m_primitives.size());
    Q_ASSERT(modelMatrices.size() == colors.size());

    int sz = modelMatrices.size();

    m_perPrimitiveCount[pidx] = sz;

    // clear previous values
    m_modelViews[pidx].clear();
    m_normals[pidx].clear();
    m_materialKDs[pidx].clear();
    // and resize
    m_modelViews[pidx].resize(16*sz);
    m_normals[pidx].resize(9*sz);
    m_materialKDs[pidx].resize(3*sz);

    for (int i = 0; i < sz; ++i)
    {
        QMatrix4x4 mv = camera->viewMatrix() * modelMatrices[i];
        QMatrix3x3 n  = mv.normalMatrix();

        for (int j = 0; j < 16; ++j)
        {
            m_modelViews[pidx][16*i+j] = mv.data()[j];
        }
        for (int j = 0; j < 9; ++j)
        {
            m_normals[pidx][9*i+j] = n.data()[j];
        }
        m_materialKDs[pidx][3*i]   = colors[i].x();
        m_materialKDs[pidx][3*i+1] = colors[i].y();
        m_materialKDs[pidx][3*i+2] = colors[i].z();
    }

    if (!m_primitiveInitialized[pidx])
    {
        setupPrimitiveBuffers(pidx);
        m_primitiveInitialized[pidx] = true;
    }
}

void BOpenGLPrimitiveInstancingManager::removePrimitive(int idx)
{
    Q_ASSERT(idx < m_primitives.size());
    m_primitives.remove(idx);
    m_primitiveInitialized.remove(idx);
    m_perPrimitiveCount.remove(idx);
    m_modelViews.remove(idx);
    m_normals.remove(idx);
    m_materialKDs.remove(idx);
    QOpenGLBuffer* buffer = m_modelViewBuffers[idx];
    m_modelViewBuffers.remove(idx);
    delete buffer;
    buffer = m_normalBuffers[idx];
    m_normalBuffers.remove(idx);
    delete buffer;
    buffer = m_materialKDBuffers[idx];
    m_materialKDBuffers.remove(idx);
    delete buffer;
}


