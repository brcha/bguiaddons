/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of bguiaddons
 */

#include "bopenglabstractprimitive.h"

#include <QtGui/QOpenGLShaderProgram>

BOpenGLAbstractPrimitive::BOpenGLAbstractPrimitive(QObject *parent)
    : QObject(parent)
    , m_vao()
    , m_positionBuffer(QOpenGLBuffer::VertexBuffer)
    , m_normalBuffer(QOpenGLBuffer::VertexBuffer)
    , m_textureCoordBuffer(QOpenGLBuffer::VertexBuffer)
    , m_tangentBuffer(QOpenGLBuffer::VertexBuffer)
    , m_indexBuffer(QOpenGLBuffer::IndexBuffer)
{
}

BOpenGLAbstractPrimitive::~BOpenGLAbstractPrimitive()
{
}

QOpenGLVertexArrayObject*BOpenGLAbstractPrimitive::vertexArrayObject()
{
    return &m_vao;
}
BOpenGLShaderManagerPtr BOpenGLAbstractPrimitive::shaderManager() const
{
    return m_shaderManager;
}

void BOpenGLAbstractPrimitive::setShaderManager(const BOpenGLShaderManagerPtr& shaderManager)
{
    if (m_shaderManager == shaderManager)
        return;
    m_shaderManager = shaderManager;
    updateVertexArrayObject();
}

void BOpenGLAbstractPrimitive::bindBuffers()
{
    QOpenGLShaderProgramPtr shader = m_shaderManager->shader();

    shader->bind();

    m_positionBuffer.bind();
    shader->enableAttributeArray("vertexPosition");
    shader->setAttributeArray("vertexPosition", GL_FLOAT, 0, 3);

    m_normalBuffer.bind();
    shader->enableAttributeArray("vertexNormal");
    shader->setAttributeArray("vertexNormal", GL_FLOAT, 0, 3);

    m_textureCoordBuffer.bind();
    shader->enableAttributeArray("vertexTexCoord");
    shader->setAttributeArray("vertexTexCoord", GL_FLOAT, 0, 2);

    m_tangentBuffer.bind();
    shader->enableAttributeArray("vertexTangent");
    shader->setAttributeArray("vertexTangent", GL_FLOAT, 0, 3);

    m_indexBuffer.bind();
}

void BOpenGLAbstractPrimitive::create()
{
    // Storage for data
    QVector<float> v;
    QVector<float> n;
    QVector<float> tex;
    QVector<float> tang;
    QVector<unsigned int> el;

    generateVertexData(v, n, tex, tang, el);

    // Create buffers
    m_positionBuffer.create();
    m_positionBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_positionBuffer.bind();
    m_positionBuffer.allocate(v.constData(), v.size() * sizeof(float));

    m_normalBuffer.create();
    m_normalBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_normalBuffer.bind();
    m_normalBuffer.allocate(n.constData(), n.size() * sizeof(float));

    m_textureCoordBuffer.create();
    m_textureCoordBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_textureCoordBuffer.bind();
    m_textureCoordBuffer.allocate(tex.constData(), tex.size() * sizeof(float));

    m_tangentBuffer.create();
    m_tangentBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_tangentBuffer.bind();
    m_tangentBuffer.allocate(tang.constData(), tang.size() * sizeof(float));

    m_indexBuffer.create();
    m_indexBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_indexBuffer.bind();
    m_indexBuffer.allocate(el.constData(), el.size() * sizeof(unsigned int));

    updateVertexArrayObject();
}

void BOpenGLAbstractPrimitive::render()
{
    m_vao.bind();

    glDrawElements(GL_TRIANGLES, indexCount(), GL_UNSIGNED_INT, 0);

    m_vao.release();
}

void BOpenGLAbstractPrimitive::updateVertexArrayObject()
{
    // sanity check
    if (!m_shaderManager || !m_positionBuffer.isCreated())
        return;

    // create vao if not yet created
    if (!m_vao.isCreated())
        m_vao.create();

    m_vao.bind();

    bindBuffers();

    m_vao.release();

    m_positionBuffer.release();
    m_normalBuffer.release();
    m_textureCoordBuffer.release();
    m_tangentBuffer.release();
    m_indexBuffer.release();
}


