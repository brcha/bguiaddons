/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of %PROJECT%
 */

#include "fbotexturenode.h"

#include "primitivesscene.h"

#include <QtGui/QOpenGLContext>
#include <QtGui/QOpenGLFramebufferObject>

FboTextureNode::FboTextureNode(QQuickWindow* parent)
    : QObject()
    , m_scene(nullptr)
    , m_window(parent)
    , m_fbo(nullptr)
    , m_texture(nullptr)
{
    connect(m_window, SIGNAL(beforeRendering())
                 ,this, SLOT(renderToTexture()));

    // Probably best to move this timer into the scene as this object is going to get recreated
    // on every resize... but for this example, that's irrelevant.
    m_time.start();
}

FboTextureNode::~FboTextureNode()
{
    delete m_fbo;
    delete m_texture;
}

BOpenGLAbstractScene* FboTextureNode::scene() const
{
    return m_scene;
}

void FboTextureNode::setScene(BOpenGLAbstractScene* scene)
{
    m_scene = scene;
}

void FboTextureNode::renderToTexture()
{
    if (!m_scene)
        return;

    // Get node's size to resize fbo and scene
    QSize size = rect().size().toSize();

    if (!m_fbo)
    {
        // Create FBO
        QOpenGLFramebufferObjectFormat format;
        format.setAttachment(QOpenGLFramebufferObject::Depth);
        m_fbo = new QOpenGLFramebufferObject(size, format);

        // Wrap the OpenGL texture in a QSGTexture object for use with the scene graph.
        m_texture = m_window->createTextureFromId(m_fbo->texture(), size);
        // Then set the texture as a simple material on this node
        setTexture(m_texture);
    }

    // Set the FBO as the render target
    m_fbo->bind();

    // Update the viewport size and render the scene
    m_scene->resize( size.width(), size.height() );
    float t = m_time.elapsed() / 1000.0f;
    m_scene->update( t ); // this should go into the scene somehow, perhaps a wrapper around the scene?
    m_scene->render();

    // Reset render target to the default framebuffer
    m_fbo->bindDefault();

    // Force a redraw of the QtQuick2 scene
    m_window->update();
}


