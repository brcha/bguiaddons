#version 330

layout (location = 0) in vec3 vertexPosition;
layout (location = 1) in vec3 vertexNormal;
layout (location = 2) in vec2 vertexTexCoord;
layout (location = 3) in mat4 modelViewMatrix; // this takes 3,4,5,6
layout (location = 7) in mat3 normalMatrix; // this takes 7,8,9
layout (location = 10) in vec3 materialKD;

out vec3 position;
out vec3 normal;
out vec3 matKD;

uniform mat4 projectionMatrix;

void main()
{
    // Calculate position by offseting vertex by instance position
    position = ( modelViewMatrix * vec4(vertexPosition, 1.0 ) ).xyz;

    // Transform the normal as usual
    normal = normalMatrix * vertexNormal;

    // Propagate materialKD
    matKD = materialKD;

    // Draw at the current position
    gl_Position = projectionMatrix * vec4( position, 1.0 );
}
