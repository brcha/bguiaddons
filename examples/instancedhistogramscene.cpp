/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of %PROJECT%
 */

#include "instancedhistogramscene.h"

#include "bopenglcamera.h"
#include "bopenglcubeprimitive.h"
#include "bopenglshadermanager.h"
#include "bopenglsphereprimitive.h"

#include <QtGui/QOpenGLFunctions_3_3_Core>
#include <QtGui/QOpenGLContext>

#include <math.h>

static const int xPoints = 100;
static const int zPoints = 100;
static const int pointCount = xPoints * zPoints;

InstancedHistogramScene::InstancedHistogramScene(QObject* parent)
    : BOpenGLAbstractScene(parent)
    , m_funcs(nullptr)
    , m_camera(new BOpenGLCamera(this))
    , m_vx(0.0f)
    , m_vy(0.0f)
    , m_vz(0.0f)
    , m_viewCenterFixed(false)
    , m_theta(0.0f)
    , m_modelMatrix()
    , m_data(3*pointCount)
    , m_sphere(nullptr)
    , m_sphereModelMatrix()
    , m_time(0.0f)
    , m_updatesEnabled(true)
{
    m_modelMatrix.setToIdentity();
    update(0.0f);

    m_camera->setPosition(QVector3D(0.0f, 0.0f, 25.0f));
    m_camera->setViewCenter(QVector3D(0.0f, 0.0f, 0.0f));
    m_camera->setUpVector(QVector3D(0.0f, 1.0f, 0.0f));

    m_camera->translateWorld(QVector3D(0.0f, 7.0f, 0.0f), BOpenGLCamera::DontTranslateViewCenter);
}

void InstancedHistogramScene::initialize()
{
    m_funcs = m_context->versionFunctions<QOpenGLFunctions_3_3_Core>();
    if (!m_funcs)
        qFatal("Could not obtain required OpenGL functions version");
    m_funcs->initializeOpenGLFunctions();

    BOpenGLShaderManagerPtr material(new BOpenGLShaderManager);
    material->setShaders(":/shaders/instancedhistogram.vert"
                        ,":/shaders/instancedhistogram.frag");

    // Create a cube
    m_cube = new BOpenGLCubePrimitive(this);
    m_cube->setShaderManager(material);
    m_cube->create();

    // create a vbo for data
    prepareVertexBuffers();
    // and vao
    prepareVertexArrayObject();


    // Noise inside a sphere
    BOpenGLShaderManagerPtr noiseMaterial(new BOpenGLShaderManager);
    noiseMaterial->setShaders(":/shaders/noise.vert"
                             ,":/shaders/noise.frag");

    m_sphere = new BOpenGLSpherePrimitive(this);
    m_sphere->setShaderManager(noiseMaterial);
    m_sphere->create();

    // enable depth testing
    glEnable(GL_DEPTH_TEST);
}

void InstancedHistogramScene::update(float t)
{
    if (m_updatesEnabled)
    {
        const float xMin = -15.0f, xMax = 15.0f;
        const float zMin = -15.0f, zMax = 15.0f;
        const float dx = (xMax - xMin) / static_cast<float>(xPoints - 1);
        const float dz = (zMax - zMin) / static_cast<float>(zPoints - 1);
        int i = 0;
        const float A = 5.0;
        for (int zi=0; zi<zPoints; ++zi)
        {
            float z = zMin + static_cast<float>(zi) * dz;

            for (int xi=0; xi<xPoints; ++xi)
            {
                float x = xMin + static_cast<float>(xi) * dx;

                double r = sqrt(x*x + z*z);
                float y = A * (sinf(1.5f*t) + cosf(1.5f*t)) * j0(r);

                m_data[3*i]   = x;
                m_data[3*i+1] = y;
                m_data[3*i+2] = z;
                ++i;
            }
        }
    }

    // Update camera
    BOpenGLCamera::CameraTranslationOption option = m_viewCenterFixed
                                           ? BOpenGLCamera::DontTranslateViewCenter
                                           : BOpenGLCamera::TranslateViewCenter;
    m_camera->translate(QVector3D(m_vx, m_vy, m_vz), option);

    if (!qFuzzyIsNull(m_panAngle))
    {
        m_camera->pan(m_panAngle);
        m_panAngle = 0.0f;
    }
    if (!qFuzzyIsNull(m_tiltAngle))
    {
        m_camera->tilt(m_tiltAngle);
        m_tiltAngle = 0.0f;
    }

    m_time = t;

    m_sphereModelMatrix.setToIdentity();
    m_sphereModelMatrix.scale(30.0f);
}

void InstancedHistogramScene::render()
{
    // qq2 disables depth
    glDepthMask(true);
    glEnable(GL_DEPTH_TEST);

    // TODO: This only works with compat profile. Find what it is that QQ2 expects in
    //       terms of vertex array state being restored.
    glPushClientAttrib( GL_CLIENT_VERTEX_ARRAY_BIT );

    glClearColor( 0.1f, 0.1f, 0.2f, 1.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    m_dataBuffer.bind();
    m_dataBuffer.allocate( m_data.data(), m_data.size() * sizeof( float ) );

    // Bind the shader program
    m_cube->shaderManager()->bind();
    QOpenGLShaderProgramPtr shader = m_cube->shaderManager()->shader();

    // Calculate needed matrices
    m_modelMatrix.setToIdentity();
    m_modelMatrix.rotate( m_theta, 0.0f, 1.0f, 0.0f );

    // We flip the view matrix since something in QQ2 inverts the y-axis
    QMatrix4x4 invertY;
    invertY.scale( 1.0f, -1.0f, 1.0f );
    QMatrix4x4 modelViewMatrix = invertY * m_camera->viewMatrix() * m_modelMatrix;
    QMatrix3x3 normalMatrix = modelViewMatrix.normalMatrix();
    shader->setUniformValue( "modelViewMatrix", modelViewMatrix );
    shader->setUniformValue( "normalMatrix", normalMatrix );
    shader->setUniformValue( "projectionMatrix", m_camera->projectionMatrix() );

    // Scale the x-z dimensions of the cuboids
    shader->setUniformValue( "cubeScale", QVector2D( 0.25f, 0.25f ) );

    // Set the lighting parameters
    shader->setUniformValue( "light.position", QVector4D( 0.0f, 0.0f, 0.0f, 1.0f ) );
    shader->setUniformValue( "light.intensity", QVector3D( 1.0f, 1.0f, 1.0f ) );
    shader->setUniformValue( "material.ks", QVector3D( 0.95f, 0.95f, 0.95f ) );
    shader->setUniformValue( "material.ka", QVector3D( 0.1f, 0.1f, 0.1f ) );
    shader->setUniformValue( "material.shininess", 100.0f );

    // Draw the cuboids
    m_cube->vertexArrayObject()->bind();
    m_funcs->glDrawElementsInstanced( GL_TRIANGLES, m_cube->indexCount(), GL_UNSIGNED_INT, 0, pointCount );
    m_cube->vertexArrayObject()->release();

    // Draw the background sphere
    m_sphere->shaderManager()->bind();
    shader = m_sphere->shaderManager()->shader();

    // Set the mvp matrix
    QMatrix4x4 mvp = m_camera->viewProjectionMatrix() * m_sphereModelMatrix;
    shader->setUniformValue( "mvp", mvp );
    shader->setUniformValue( "time", m_time );

    // Let the mesh setup the remainder of its state and draw itself
    m_sphere->render();

    glPopClientAttrib();

    glDisable(GL_DEPTH_TEST);
    glDepthMask(false);
}

void InstancedHistogramScene::resize(int w, int h)
{
    glViewport(0, 0, w, h);

    float aspect = static_cast<float>(w) / static_cast<float>(h);
    m_camera->setPerspectiveProjection(60.0f, aspect, 0.3f, 100.0f);
}

void InstancedHistogramScene::toggleUpdates()
{
    m_updatesEnabled = !m_updatesEnabled;
}

void InstancedHistogramScene::setSideSpeed(float vx)
{
    m_vx = vx;
}

void InstancedHistogramScene::setVerticalSpeed(float vy)
{
    m_vy = vy;
}

void InstancedHistogramScene::setForwardSpeed(float vz)
{
    m_vz = vz;
}

void InstancedHistogramScene::setViewCenterFixed(bool fixed)
{
    m_viewCenterFixed = fixed;
}

void InstancedHistogramScene::pan(float angle)
{
    m_panAngle = angle;
}

void InstancedHistogramScene::tilt(float angle)
{
    m_tiltAngle = angle;
}

void InstancedHistogramScene::prepareVertexBuffers()
{
    m_dataBuffer.create();
    m_dataBuffer.setUsagePattern( QOpenGLBuffer::DynamicDraw );
    m_dataBuffer.bind();
    m_dataBuffer.allocate( m_data.data(), m_data.size() * sizeof( float ) );
}

void InstancedHistogramScene::prepareVertexArrayObject()
{
    // Bind the marker's VAO
    m_cube->vertexArrayObject()->bind();

    // Enable the data buffer and add it to the marker's VAO
    QOpenGLShaderProgramPtr shader = m_cube->shaderManager()->shader();
    shader->bind();
    m_dataBuffer.bind();
    shader->enableAttributeArray( "point" );
    shader->setAttributeBuffer( "point", GL_FLOAT, 0, 3 );

    // We only vary the point attribute once per instance
    GLuint pointLocation = shader->attributeLocation( "point" );
    m_funcs->glVertexAttribDivisor( pointLocation, 1 );

    m_cube->vertexArrayObject()->release();
}
