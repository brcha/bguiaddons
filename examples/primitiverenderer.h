/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of %PROJECT%
 */

#ifndef PRIMITIVERENDERER_H
#define PRIMITIVERENDERER_H

#include <QtQuick/QQuickItem>

class BOpenGLAbstractScene;

class PrimitiveRenderer : public QQuickItem
{
    Q_OBJECT
public:
    explicit PrimitiveRenderer(QQuickItem * parent = 0);
    virtual ~PrimitiveRenderer();

protected:
    QSGNode * updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *);

private:
    BOpenGLAbstractScene * m_scene;
};

#endif // PRIMITIVERENDERER_H
