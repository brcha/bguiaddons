/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of %PROJECT%
 */

#ifndef MANAGEDSCENE_H
#define MANAGEDSCENE_H

#include "bopenglabstractscene.h"

class BOpenGLPrimitiveRenderer;
class BOpenGLCamera;

class ManagedScene : public BOpenGLAbstractScene
{
    Q_OBJECT
public:
    explicit ManagedScene(QObject *parent = 0);
    ~ManagedScene();

    virtual void initialize();
    virtual void update(float t);
    virtual void render();
    virtual void resize(int w, int h);

private:
    BOpenGLPrimitiveRenderer * m_renderer;

    enum {
        CubeID
       ,SphereID
       ,CylinderID
       ,GroundID
    };

    float m_theta;

    BOpenGLCamera * m_camera;
};

#endif // MANAGEDSCENE_H
