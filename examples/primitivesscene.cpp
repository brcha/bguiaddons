/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of %PROJECT%
 */

#include "primitivesscene.h"

#include "bopenglcamera.h"
#include "bopenglshadermanager.h"
#include "bopenglcubeprimitive.h"
#include "bopenglsphereprimitive.h"

#include <QtGui/QOpenGLContext>
#include <QtGui/QOpenGLFunctions_3_3_Core>

#include <math.h>

static const int pointCount = 400;

PrimitivesScene::PrimitivesScene(QObject* parent)
    : BOpenGLAbstractScene(parent)
    , m_funcs(nullptr)
    , m_camera(new BOpenGLCamera(this))
    , m_vx(0.0f)
    , m_vy(0.0f)
    , m_vz(0.0f)
    , m_viewCenterFixed(false)
    , m_panAngle(0.0f)
    , m_tiltAngle(0.0f)
    , m_updatesEnabled(true)
    , m_cube(nullptr)
    , m_sphere(nullptr)
    , m_theta(0.0f)
    , m_modelMatrix()
    , m_data1(3 * pointCount)
    , m_data2(3 * pointCount)
{
    m_modelMatrix.setToIdentity();
    update(0.0f);

    // Init camera
    m_camera->setPosition(QVector3D(0.0f, 0.0f, 20.0f));
    m_camera->setViewCenter(QVector3D(0.0f, 0.0f, 0.0f));
    m_camera->setUpVector(QVector3D(0.0f, 1.0f, 0.0f));

//    m_camera->translateWorld( QVector3D( 4.0f, 7.0f, 0.0f ), Camera::DontTranslateViewCenter );
}

PrimitivesScene::~PrimitivesScene()
{
}

void PrimitivesScene::initialize()
{
    m_funcs = m_context->versionFunctions<QOpenGLFunctions_3_3_Core>();
    if (!m_funcs)
        qFatal("Couldn't initialize OpenGL functions!");

    m_funcs->initializeOpenGLFunctions();

    BOpenGLShaderManagerPtr material(new BOpenGLShaderManager);
//    material->setShaders(":/shaders/shader.vert.glsl"
//                        ,":/shaders/shader.frag.glsl");
    material->setShaders(":/shaders/instancedgeometry.vert"
                        ,":/shaders/instancedgeometry.frag");

    m_cube = new BOpenGLCubePrimitive(this);
    m_cube->setShaderManager(material);
    m_cube->create();

    m_sphere = new BOpenGLSpherePrimitive(this);
    m_sphere->setShaderManager(material);
    m_sphere->create();

    prepareVertexBuffers();

    prepareVertexArrayObjects();

    glEnable(GL_DEPTH_TEST);

    glEnable(GL_CULL_FACE);

    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
}

void PrimitivesScene::update(float t)
{
    if (!m_updatesEnabled)
        return;

    const float xMin = -150.0f, xMax = 150.0f;
    const float dx = (xMax - xMin) / static_cast<float>(pointCount - 1);

    for (int i=0; i < pointCount; ++i)
    {
        float x = xMin + static_cast<float>(i) * dx;
        float y1 = 6.0f * sinf(0.4f * x - 0.4f * t);
        float y2 = 4.0f * cosf(0.7f * x - 0.2f * t);

        m_data1[3*i]   = x;
        m_data1[3*i+1] = y1;
        m_data1[3*i+2] = 0.0f;

        m_data2[3*i]   = x;
        m_data2[3*i+1] = y2;
        m_data2[3*i+2] = 3.0f;
    }

    BOpenGLCamera::CameraTranslationOption option = m_viewCenterFixed ? BOpenGLCamera::DontTranslateViewCenter
                                                               : BOpenGLCamera::TranslateViewCenter;

    m_camera->translate(QVector3D(m_vx, m_vy, m_vz), option);

    if (!qFuzzyIsNull(m_panAngle))
    {
        m_camera->pan(m_panAngle);
        m_panAngle = 0.0f; // reset so that camera doesn't go berzerk
    }

    if (!qFuzzyIsNull(m_tiltAngle))
    {
        m_camera->tilt(m_tiltAngle);
        m_tiltAngle = 0.0f;
    }
}

void PrimitivesScene::render()
{
    // QtQuick2 disables depth test, so reenable it here
//    glDepthMask(true);
//    glEnable(GL_DEPTH_TEST);
//    glEnable(GL_CULL_FACE);

//    // QQ2 requires VAOs to be restored
//    glPushClientAttrib(GL_CLIENT_VERTEX_ARRAY_BIT);

//    glClearColor( 1.0f, 1.0f, 1.0f, 1.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    m_dataBuffer1.bind();
    m_dataBuffer1.allocate(m_data1.data(), m_data1.size() * sizeof(float));

    m_dataBuffer2.bind();
    m_dataBuffer2.allocate(m_data2.data(), m_data2.size() * sizeof(float));

    m_cube->shaderManager()->bind();
    QOpenGLShaderProgramPtr shader = m_cube->shaderManager()->shader();

    m_modelMatrix.setToIdentity();
    m_modelMatrix.rotate(m_theta, 0.0f, 1.0f, 0.0f);
    m_theta += 0.1f;

    // TODO: set some QtQuick2 flag to camera, since this reversing is annoying
//    m_camera->setStandardUniforms(shader, m_modelMatrix);
    // We flip the view matrix since something in QQ2 inverts the y-axis
    QMatrix4x4 invertY;
    invertY.scale( 1.0f, -1.0f, 1.0f );
    QMatrix4x4 modelViewMatrix = invertY * m_camera->viewMatrix() * m_modelMatrix;
    QMatrix3x3 normalMatrix = modelViewMatrix.normalMatrix();
    shader->setUniformValue( "modelViewMatrix", modelViewMatrix );
    shader->setUniformValue( "normalMatrix", normalMatrix );
    shader->setUniformValue( "projectionMatrix", m_camera->projectionMatrix() );

    // Setup lighting
    shader->setUniformValue( "light.position", QVector4D( 0.0f, 0.0f, 0.0f, 1.0f ) );
    shader->setUniformValue( "light.intensity", QVector3D( 1.0f, 1.0f, 1.0f ) );
    shader->setUniformValue( "material.kd", QVector3D( 0.0f, 0.6f, 0.1f ) );
    shader->setUniformValue( "material.ks", QVector3D( 0.95f, 0.95f, 0.95f ) );
    shader->setUniformValue( "material.ka", QVector3D( 0.1f, 0.1f, 0.1f ) );
    shader->setUniformValue( "material.shininess", 100.0f );

//    m_cube->render();
    m_cube->vertexArrayObject()->bind();
    m_funcs->glDrawElementsInstanced(GL_TRIANGLES, m_cube->indexCount(), GL_UNSIGNED_INT, 0, pointCount);
    m_cube->vertexArrayObject()->release();

    shader->setUniformValue( "material.kd", QVector3D( 0.9f, 0.0f, 0.0f ) );
    m_sphere->vertexArrayObject()->bind();
    m_funcs->glDrawElementsInstanced(GL_TRIANGLES, m_sphere->indexCount(), GL_UNSIGNED_INT, 0, pointCount);
    m_sphere->vertexArrayObject()->release();
//    m_sphere->render();

    // Restore QtQuick2 state
//    glPopClientAttrib();

//    glDisable(GL_CULL_FACE);
//    glDisable(GL_DEPTH_TEST);
//    glDepthMask(false);
}

void PrimitivesScene::resize(int w, int h)
{
    glViewport(0, 0, w, h);

    float aspect = static_cast<float>(w) / static_cast<float>(h);
    m_camera->setPerspectiveProjection(60.0f, aspect, 0.3f, 1000.0f);
}

void PrimitivesScene::toggleUpdates()
{
    m_updatesEnabled = ! m_updatesEnabled;
}

void PrimitivesScene::setSideSpeed(float vx)
{
    m_vx = vx;
}

void PrimitivesScene::setVerticalSpeed(float vy)
{
    m_vy = vy;
}

void PrimitivesScene::setForwardSpeed(float vz)
{
    m_vz = vz;
}

void PrimitivesScene::setViewCenterFixed(bool fixed)
{
    m_viewCenterFixed = fixed;
}

void PrimitivesScene::pan(float angle)
{
    m_panAngle = angle;
}

void PrimitivesScene::tilt(float angle)
{
    m_tiltAngle = angle;
}

void PrimitivesScene::prepareVertexBuffers()
{
    m_dataBuffer1.create();
    m_dataBuffer1.setUsagePattern(QOpenGLBuffer::DynamicDraw);
    m_dataBuffer1.bind();
    m_dataBuffer1.allocate(m_data1.data(), m_data1.size() * sizeof(float));

    m_dataBuffer2.create();
    m_dataBuffer2.setUsagePattern(QOpenGLBuffer::DynamicDraw);
    m_dataBuffer2.bind();
    m_dataBuffer2.allocate(m_data2.data(), m_data2.size() * sizeof(float));
}

void PrimitivesScene::prepareVertexArrayObjects()
{
    QOpenGLShaderProgramPtr shader = m_cube->shaderManager()->shader();
    shader->bind();

    m_cube->vertexArrayObject()->bind();
    m_dataBuffer1.bind();
    shader->enableAttributeArray("point");
    shader->setAttributeArray("point", GL_FLOAT, 0, 3);
    GLuint pointLocation = shader->attributeLocation("point");
    m_funcs->glVertexAttribDivisor(pointLocation, 1);
    m_cube->vertexArrayObject()->release();

    m_sphere->vertexArrayObject()->bind();
    m_dataBuffer2.bind();
    shader->enableAttributeArray("point");
    shader->setAttributeArray("point", GL_FLOAT, 0, 3);
    m_funcs->glVertexAttribDivisor(pointLocation, 1);
    m_sphere->vertexArrayObject()->release();
}

