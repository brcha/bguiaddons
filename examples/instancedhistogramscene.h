/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of %PROJECT%
 */

#ifndef INSTANCEDHISTOGRAMSCENE_H
#define INSTANCEDHISTOGRAMSCENE_H

#include "bopenglabstractscene.h"

#include <QtGui/QOpenGLBuffer>
#include <QtGui/QMatrix4x4>

class BOpenGLCamera;
class BOpenGLCubePrimitive;
class BOpenGLSpherePrimitive;

class QOpenGLFunctions_3_3_Core;

class InstancedHistogramScene : public BOpenGLAbstractScene
{
    Q_OBJECT
public:
    InstancedHistogramScene(QObject * parent = 0);

    virtual void initialize();
    virtual void update(float t);
    virtual void render();
    virtual void resize(int w, int h);

    void toggleUpdates();

    void setSideSpeed(float vx);
    void setVerticalSpeed(float vy);
    void setForwardSpeed(float vz);
    void setViewCenterFixed(bool fixed);

    void pan(float angle);
    void tilt(float angle);

private:
    void prepareVertexBuffers();
    void prepareVertexArrayObject();

private:
    QOpenGLFunctions_3_3_Core * m_funcs;

    BOpenGLCamera * m_camera;
    float m_vx;
    float m_vy;
    float m_vz;
    bool m_viewCenterFixed;
    float m_panAngle;
    float m_tiltAngle;

    BOpenGLCubePrimitive * m_cube;

    float m_theta;
    QMatrix4x4 m_modelMatrix;

    QVector<float> m_data;
    QOpenGLBuffer m_dataBuffer;

    BOpenGLSpherePrimitive * m_sphere;
    QMatrix4x4 m_sphereModelMatrix;
    float m_time;

    bool m_updatesEnabled;
};

#endif // INSTANCEDHISTOGRAMSCENE_H
