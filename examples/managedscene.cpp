/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of %PROJECT%
 */

#include "managedscene.h"

#include "bopenglcamera.h"
#include "bopenglshadermanager.h"
#include "bopenglcubeprimitive.h"
#include "bopenglsphereprimitive.h"
#include "bopenglcylinderprimitive.h"
#include "bopenglplaneprimitive.h"
#include "bopenglprimitiverenderer.h"
#include "bopenglprimitiveinstancingmanager.h"

#include <QtGui/QOpenGLContext>

#include <cmath>

static const int pointCount = 400;

ManagedScene::ManagedScene(QObject *parent)
    : BOpenGLAbstractScene(parent)
    , m_renderer(new BOpenGLPrimitiveRenderer(this))
    , m_theta(0.0f)
    , m_camera(new BOpenGLCamera(this))
{
    m_camera->setInQml(true);
    m_camera->setPosition(QVector3D(0.0f, 0.0f, 20.0f));
    m_camera->setViewCenter(QVector3D(0.0f, 0.0f, 0.0f));
    m_camera->setUpVector(QVector3D(0.0f, 1.0f, 0.0f));
}

ManagedScene::~ManagedScene()
{
}

void ManagedScene::initialize()
{
    BOpenGLShaderManagerPtr shader(new BOpenGLShaderManager);
    shader->setShaders(":/shaders/instancingmgr.vert.glsl"
                      ,":/shaders/instancingmgr.frag.glsl");

    BOpenGLPrimitiveInstancingManager * instMgr = new BOpenGLPrimitiveInstancingManager(this);
    instMgr->setContext(m_context);
    instMgr->initialize();
    instMgr->setShaderManager(shader);
    m_renderer->setInstancingManager(instMgr);

    BOpenGLCubePrimitive * cube = new BOpenGLCubePrimitive(this);
    cube->setShaderManager(shader);
    cube->create();
    m_renderer->addInstancedPrimitive(CubeID, cube);
    m_renderer->allocateInstancedPrimitive(CubeID, pointCount);

    BOpenGLSpherePrimitive * sphere = new BOpenGLSpherePrimitive(this);
    sphere->setShaderManager(shader);
    sphere->create();
    m_renderer->addInstancedPrimitive(SphereID, sphere);
    m_renderer->allocateInstancedPrimitive(SphereID, pointCount);

    BOpenGLCylinderPrimitive * cylinder = new BOpenGLCylinderPrimitive(this);
    cylinder->setShaderManager(shader);
    cylinder->create();
    m_renderer->addInstancedPrimitive(CylinderID, cylinder);
    m_renderer->allocateInstancedPrimitive(CylinderID, pointCount);

    BOpenGLShaderManagerPtr planeShader(new BOpenGLShaderManager);
    planeShader->setShaders(":/shaders/shader.vert.glsl"
                           ,":/shaders/shader.frag.glsl");

    BOpenGLPlanePrimitive * ground = new BOpenGLPlanePrimitive(50.0f, 50.0f, 100, 100, this);
    ground->setShaderManager(planeShader);
    ground->create();
    m_renderer->addSinglePrimitive(GroundID, ground);

    QMatrix4x4 planeModel;
    planeModel.setToIdentity();
    planeModel.translate(0.0f, -7.0f, 0.0f);
    m_renderer->setSingleMatrix(GroundID, planeModel);
    m_renderer->setSingleColor(GroundID, QVector3D(0.0f, 0.1f, 0.9f));

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

    update(0.0f);
}

void ManagedScene::update(float t)
{
    const float xMin = -150.0f, xMax = 150.0f;
    const float dx = (xMax - xMin) / static_cast<float>(pointCount - 1);

    m_theta += 0.1f;

    for (int i=0; i < pointCount; ++i)
    {
        float x = xMin + static_cast<float>(i) * dx;
        float y1 = 6.0f * sinf(0.4f * x - 0.4f * t);
        float y2 = 4.0f * cosf(0.7f * x - 0.2f * t);
        float y3 = 7.0f * cosf(0.3f * x - 0.8f * t);
        float col = 0.5f + 0.5f * cosf(0.6f * x - 0.1f * t);

        QMatrix4x4 cubeModelMatrix;
        cubeModelMatrix.setToIdentity();
        cubeModelMatrix.rotate(m_theta, 0.0f, 1.0f, 0.0f);
        cubeModelMatrix.translate(x, y1, 0.0f);

        m_renderer->setInstancedMatrix(CubeID, i, cubeModelMatrix);
        m_renderer->setInstancedColor(CubeID, i, QVector3D(0.0f, col, 0.1f));

        QMatrix4x4 sphereModelMatrix;
        sphereModelMatrix.setToIdentity();
        sphereModelMatrix.rotate(2*m_theta, 0.0f, 1.0f, 0.0f);
        sphereModelMatrix.translate(x, y2, 0.0f);

        m_renderer->setInstancedMatrix(SphereID, i, sphereModelMatrix);
        m_renderer->setInstancedColor(SphereID, i, QVector3D(col, 0.0f, 0.1f));

        QMatrix4x4 cylinderModelMatrix;
        cylinderModelMatrix.setToIdentity();
        cylinderModelMatrix.rotate(m_theta / 2.0f, 0.0f, 1.0f, 0.0f);
        cylinderModelMatrix.translate(x, y3, 0.0f);

        m_renderer->setInstancedMatrix(CylinderID, i, cylinderModelMatrix);
        m_renderer->setInstancedColor(CylinderID, i, QVector3D(1.0f - col, 0.3f, 0.6f));
    }
}

void ManagedScene::render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_renderer->instancingShader()->bind();
    QOpenGLShaderProgramPtr shader = m_renderer->instancingShader()->shader();

    shader->setUniformValue("projectionMatrix", m_camera->projectionMatrix());

    // Setup lighting
    shader->setUniformValue( "light.position", QVector4D( 0.0f, 0.0f, 0.0f, 1.0f ) );
    shader->setUniformValue( "light.intensity", QVector3D( 1.0f, 1.0f, 1.0f ) );
    shader->setUniformValue( "material.kd", QVector3D( 0.0f, 0.6f, 0.1f ) );
    shader->setUniformValue( "material.ks", QVector3D( 0.95f, 0.95f, 0.95f ) );
    shader->setUniformValue( "material.ka", QVector3D( 0.1f, 0.1f, 0.1f ) );
    shader->setUniformValue( "material.shininess", 100.0f );

    m_renderer->singleShader(GroundID)->bind();
    shader = m_renderer->singleShader(GroundID)->shader();

    // Setup lighting
    shader->setUniformValue( "light.position", QVector4D( 0.0f, 0.0f, 0.0f, 1.0f ) );
    shader->setUniformValue( "light.intensity", QVector3D( 1.0f, 1.0f, 1.0f ) );
    shader->setUniformValue( "material.kd", QVector3D(0.0f, 0.1f, 0.9f) );
    shader->setUniformValue( "material.ks", QVector3D( 0.95f, 0.95f, 0.95f ) );
    shader->setUniformValue( "material.ka", QVector3D( 0.1f, 0.1f, 0.1f ) );
    shader->setUniformValue( "material.shininess", 100.0f );

    // Now all params are set in both shaders. Go render everything:
    m_renderer->render(m_camera);
}

void ManagedScene::resize(int w, int h)
{
    glViewport(0, 0, w, h);

    float aspect = static_cast<float>(w) / static_cast<float>(h);
    m_camera->setPerspectiveProjection(60.0f, aspect, 0.3f, 1000.0f);
}

