/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of %PROJECT%
 */

#ifndef FBOTEXTURENODE_H
#define FBOTEXTURENODE_H

#include <QtCore/QObject>
#include <QtQuick/QSGSimpleTextureNode>
#include <QtQuick/QQuickWindow>
#include <QtCore/QTime>

class BOpenGLAbstractScene;

class FboTextureNode : public QObject, public QSGSimpleTextureNode
{
    Q_OBJECT
public:
    explicit FboTextureNode(QQuickWindow *parent = 0);
    virtual ~FboTextureNode();

    BOpenGLAbstractScene* scene() const;
    void setScene(BOpenGLAbstractScene* scene);

private slots:
    void renderToTexture();

private:
    BOpenGLAbstractScene * m_scene;

    QQuickWindow * m_window;
    QOpenGLFramebufferObject * m_fbo;
    QSGTexture * m_texture;

    QTime m_time;
};

#endif // FBOTEXTURENODE_H
