/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of %PROJECT%
 */

#include "multipleinstancescene.h"

#include "bopenglcamera.h"
#include "bopenglshadermanager.h"
#include "bopenglcubeprimitive.h"
#include "bopenglsphereprimitive.h"
#include "bopenglplaneprimitive.h"
#include "bopenglcylinderprimitive.h"

#include <QtGui/QOpenGLContext>
#include <QtGui/QOpenGLFunctions_3_3_Core>

#include <math.h>

static const int pointCount = 400;

MultipleInstanceScene::MultipleInstanceScene(QObject* parent)
    : BOpenGLAbstractScene(parent)
    , m_funcs(nullptr)
    , m_camera(new BOpenGLCamera(this))
    , m_vx(0.0f)
    , m_vy(0.0f)
    , m_vz(0.0f)
    , m_viewCenterFixed(false)
    , m_panAngle(0.0f)
    , m_tiltAngle(0.0f)
    , m_updatesEnabled(true)
    , m_cube(nullptr)
    , m_sphere(nullptr)
    , m_plane(nullptr)
    , m_cylinder(nullptr)
    , m_theta(0.0f)
    , m_modelView1(16*pointCount)
    , m_normal1(9*pointCount)
    , m_modelView2(16*pointCount)
    , m_normal2(9*pointCount)
    , m_modelView3(16*pointCount)
    , m_normal3(9*pointCount)
{
    // Init camera
    m_camera->setInQml(true);
    m_camera->setPosition(QVector3D(0.0f, 0.0f, 20.0f));
    m_camera->setViewCenter(QVector3D(0.0f, 0.0f, 0.0f));
    m_camera->setUpVector(QVector3D(0.0f, 1.0f, 0.0f));

    //    m_camera->translateWorld( QVector3D( 4.0f, 7.0f, 0.0f ), Camera::DontTranslateViewCenter );

    update(0.0f);
}

MultipleInstanceScene::~MultipleInstanceScene()
{
}

void MultipleInstanceScene::initialize()
{
    m_funcs = m_context->versionFunctions<QOpenGLFunctions_3_3_Core>();
    if (!m_funcs)
        qFatal("Couldn't initialize OpenGL functions!");

    m_funcs->initializeOpenGLFunctions();

    BOpenGLShaderManagerPtr material(new BOpenGLShaderManager);
//    material->setShaders(":/shaders/shader.vert.glsl"
//                        ,":/shaders/shader.frag.glsl");
    material->setShaders(":/shaders/multipleinstances.vert.glsl"
                        ,":/shaders/multipleinstances.frag.glsl");

    m_cube = new BOpenGLCubePrimitive(this);
    m_cube->setShaderManager(material);
    m_cube->create();

    m_sphere = new BOpenGLSpherePrimitive(this);
    m_sphere->setShaderManager(material);
    m_sphere->create();

    m_cylinder = new BOpenGLCylinderPrimitive(this);
    m_cylinder->setShaderManager(material);
    m_cylinder->create();

    BOpenGLShaderManagerPtr planeMaterial(new BOpenGLShaderManager);
    planeMaterial->setShaders(":/shaders/shader.vert.glsl"
                             ,":/shaders/shader.frag.glsl");
    m_plane = new BOpenGLPlanePrimitive(50.0f, 50.0f, 100, 100, this);
    m_plane->setShaderManager(planeMaterial);
    m_plane->create();

    prepareVertexBuffers();

    prepareVertexArrayObjects();

    glEnable(GL_DEPTH_TEST);

    glEnable(GL_CULL_FACE);

    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
}

void MultipleInstanceScene::update(float t)
{
    if (!m_updatesEnabled)
        return;

    const float xMin = -150.0f, xMax = 150.0f;
    const float dx = (xMax - xMin) / static_cast<float>(pointCount - 1);

//    QMatrix4x4 invertY;
//    invertY.scale( 1.0f, -1.0f, 1.0f );

    m_theta += 0.1f;

    for (int i=0; i < pointCount; ++i)
    {
        float x = xMin + static_cast<float>(i) * dx;
        float y1 = 6.0f * sinf(0.4f * x - 0.4f * t);
        float y2 = 4.0f * cosf(0.7f * x - 0.2f * t);
        float y3 = 7.0f * cosf(0.3f * x - 0.8f * t);

        QMatrix4x4 cubeModelMatrix;
        cubeModelMatrix.setToIdentity();
        cubeModelMatrix.rotate(m_theta, 0.0f, 1.0f, 0.0f);
        cubeModelMatrix.translate(x, y1, 0.0f);
        QMatrix4x4 mv = /*invertY * */m_camera->viewMatrix() * cubeModelMatrix;
        QMatrix3x3 n = mv.normalMatrix();

        for (int j=0; j<16; ++j)
        {
            m_modelView1[16*i+j] = mv.data()[j];
        }
        for (int j=0; j<9; ++j)
        {
            m_normal1[9*i+j] = n.data()[j];
        }

        QMatrix4x4 sphereModelMatrix;
        sphereModelMatrix.setToIdentity();
        sphereModelMatrix.rotate(2*m_theta, 0.0f, 1.0f, 0.0f);
        sphereModelMatrix.translate(x, y2, 0.0f);
        mv = /*invertY * */m_camera->viewMatrix() * sphereModelMatrix;
        n = mv.normalMatrix();

        for (int j=0; j<16; ++j)
        {
            m_modelView2[16*i+j] = mv.data()[j];
        }
        for (int j=0; j<9; ++j)
        {
            m_normal2[9*i+j] = n.data()[j];
        }

        QMatrix4x4 cylinderModelMatrix;
        cylinderModelMatrix.setToIdentity();
        cylinderModelMatrix.rotate(m_theta / 2.0f, 0.0f, 1.0f, 0.0f);
        cylinderModelMatrix.translate(x, y3, 0.0f);
        mv = /*invertY * */m_camera->viewMatrix() * cylinderModelMatrix;
        n = mv.normalMatrix();

        for (int j=0; j<16; ++j)
        {
            m_modelView3[16*i+j] = mv.data()[j];
        }
        for (int j=0; j<9; ++j)
        {
            m_normal3[9*i+j] = n.data()[j];
        }
    }

    BOpenGLCamera::CameraTranslationOption option = m_viewCenterFixed ? BOpenGLCamera::DontTranslateViewCenter
                                                               : BOpenGLCamera::TranslateViewCenter;

    m_camera->translate(QVector3D(m_vx, m_vy, m_vz), option);

    if (!qFuzzyIsNull(m_panAngle))
    {
        m_camera->pan(m_panAngle);
        m_panAngle = 0.0f; // reset so that camera doesn't go berzerk
    }

    if (!qFuzzyIsNull(m_tiltAngle))
    {
        m_camera->tilt(m_tiltAngle);
        m_tiltAngle = 0.0f;
    }
}

void MultipleInstanceScene::render()
{
    // QtQuick2 disables depth test, so reenable it here
//    glDepthMask(true);
//    glEnable(GL_DEPTH_TEST);
//    glEnable(GL_CULL_FACE);

//    // QQ2 requires VAOs to be restored
//    glPushClientAttrib(GL_CLIENT_VERTEX_ARRAY_BIT);

//    glClearColor( 1.0f, 1.0f, 1.0f, 1.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    m_modelViewBuffer1.bind();
    m_modelViewBuffer1.allocate(m_modelView1.data(), m_modelView1.size() * sizeof(float));

    m_normalBuffer1.bind();
    m_normalBuffer1.allocate(m_normal1.data(), m_normal1.size() * sizeof(float));

    m_modelViewBuffer2.bind();
    m_modelViewBuffer2.allocate(m_modelView2.data(), m_modelView2.size() * sizeof(float));

    m_normalBuffer2.bind();
    m_normalBuffer2.allocate(m_normal2.data(), m_normal2.size() * sizeof(float));

    m_modelViewBuffer3.bind();
    m_modelViewBuffer3.allocate(m_modelView3.data(), m_modelView3.size() * sizeof(float));

    m_normalBuffer3.bind();
    m_normalBuffer3.allocate(m_normal3.data(), m_normal3.size() * sizeof(float));

    m_cube->shaderManager()->bind();
    QOpenGLShaderProgramPtr shader = m_cube->shaderManager()->shader();

    // TODO: set some QtQuick2 flag to camera, since this reversing is annoying
//    m_camera->setStandardUniforms(shader, m_modelMatrix);
    // We flip the view matrix since something in QQ2 inverts the y-axis
//    QMatrix4x4 invertY;
//    invertY.scale( 1.0f, -1.0f, 1.0f );
    shader->setUniformValue( "projectionMatrix", /*invertY * */m_camera->projectionMatrix() );

    // Setup lighting
    shader->setUniformValue( "light.position", QVector4D( 0.0f, 0.0f, 0.0f, 1.0f ) );
    shader->setUniformValue( "light.intensity", QVector3D( 1.0f, 1.0f, 1.0f ) );
    shader->setUniformValue( "material.kd", QVector3D( 0.0f, 0.6f, 0.1f ) );
    shader->setUniformValue( "material.ks", QVector3D( 0.95f, 0.95f, 0.95f ) );
    shader->setUniformValue( "material.ka", QVector3D( 0.1f, 0.1f, 0.1f ) );
    shader->setUniformValue( "material.shininess", 100.0f );

//    m_cube->render();
    m_cube->vertexArrayObject()->bind();
    m_funcs->glDrawElementsInstanced(GL_TRIANGLES, m_cube->indexCount(), GL_UNSIGNED_INT, 0, pointCount);
    m_cube->vertexArrayObject()->release();

    shader->setUniformValue( "material.kd", QVector3D( 0.9f, 0.0f, 0.0f ) );
    m_sphere->vertexArrayObject()->bind();
    m_funcs->glDrawElementsInstanced(GL_TRIANGLES, m_sphere->indexCount(), GL_UNSIGNED_INT, 0, pointCount);
    m_sphere->vertexArrayObject()->release();
//    m_sphere->render();

    shader->setUniformValue( "material.kd", QVector3D( 0.9f, 0.3f, 0.6f ) );
    m_cylinder->vertexArrayObject()->bind();
    m_funcs->glDrawElementsInstanced(GL_TRIANGLES, m_cylinder->indexCount(), GL_UNSIGNED_INT, 0, pointCount);
    m_cylinder->vertexArrayObject()->release();

    // Draw plane
    m_plane->shaderManager()->bind();
    shader = m_plane->shaderManager()->shader();

    // TODO: set some QtQuick2 flag to camera, since this reversing is annoying
//    m_camera->setStandardUniforms(shader, m_modelMatrix);
    // We flip the view matrix since something in QQ2 inverts the y-axis
    shader->setUniformValue( "projectionMatrix", /*invertY * */m_camera->projectionMatrix() );

    // Setup lighting
    shader->setUniformValue( "light.position", QVector4D( 0.0f, 0.0f, 0.0f, 1.0f ) );
    shader->setUniformValue( "light.intensity", QVector3D( 1.0f, 1.0f, 1.0f ) );
    shader->setUniformValue( "material.kd", QVector3D(0.0f, 0.1f, 0.9f) );
    shader->setUniformValue( "material.ks", QVector3D( 0.95f, 0.95f, 0.95f ) );
    shader->setUniformValue( "material.ka", QVector3D( 0.1f, 0.1f, 0.1f ) );
    shader->setUniformValue( "material.shininess", 100.0f );


    QMatrix4x4 planeModel;
    planeModel.setToIdentity();
    planeModel.translate(0.0f, -7.0f, 0.0f);
    QMatrix4x4 modelView = /* invertY * */m_camera->viewMatrix() * planeModel;
    shader->setUniformValue("modelViewMatrix", modelView);
    QMatrix3x3 normalMatrix = modelView.normalMatrix();
    shader->setUniformValue("normalMatrix", normalMatrix);
    m_plane->render();

    // Restore QtQuick2 state
//    glPopClientAttrib();

//    glDisable(GL_CULL_FACE);
//    glDisable(GL_DEPTH_TEST);
//    glDepthMask(false);
}

void MultipleInstanceScene::resize(int w, int h)
{
    glViewport(0, 0, w, h);

    float aspect = static_cast<float>(w) / static_cast<float>(h);
    m_camera->setPerspectiveProjection(60.0f, aspect, 0.3f, 1000.0f);
}

void MultipleInstanceScene::toggleUpdates()
{
    m_updatesEnabled = ! m_updatesEnabled;
}

void MultipleInstanceScene::setSideSpeed(float vx)
{
    m_vx = vx;
}

void MultipleInstanceScene::setVerticalSpeed(float vy)
{
    m_vy = vy;
}

void MultipleInstanceScene::setForwardSpeed(float vz)
{
    m_vz = vz;
}

void MultipleInstanceScene::setViewCenterFixed(bool fixed)
{
    m_viewCenterFixed = fixed;
}

void MultipleInstanceScene::pan(float angle)
{
    m_panAngle = angle;
}

void MultipleInstanceScene::tilt(float angle)
{
    m_tiltAngle = angle;
}

void MultipleInstanceScene::prepareVertexBuffers()
{
    m_modelViewBuffer1.create();
    m_modelViewBuffer1.setUsagePattern(QOpenGLBuffer::DynamicDraw);
    m_modelViewBuffer1.bind();
    m_modelViewBuffer1.allocate(m_modelView1.data(), m_modelView1.size() * sizeof(float));

    m_normalBuffer1.create();
    m_normalBuffer1.setUsagePattern(QOpenGLBuffer::DynamicDraw);
    m_normalBuffer1.bind();
    m_normalBuffer1.allocate(m_normal1.data(), m_normal1.size() * sizeof(float));

    m_modelViewBuffer2.create();
    m_modelViewBuffer2.setUsagePattern(QOpenGLBuffer::DynamicDraw);
    m_modelViewBuffer2.bind();
    m_modelViewBuffer2.allocate(m_modelView2.data(), m_modelView2.size() * sizeof(float));

    m_normalBuffer2.create();
    m_normalBuffer2.setUsagePattern(QOpenGLBuffer::DynamicDraw);
    m_normalBuffer2.bind();
    m_normalBuffer2.allocate(m_normal2.data(), m_normal2.size() * sizeof(float));

    m_modelViewBuffer3.create();
    m_modelViewBuffer3.setUsagePattern(QOpenGLBuffer::DynamicDraw);
    m_modelViewBuffer3.bind();
    m_modelViewBuffer3.allocate(m_modelView3.data(), m_modelView3.size() * sizeof(float));

    m_normalBuffer3.create();
    m_normalBuffer3.setUsagePattern(QOpenGLBuffer::DynamicDraw);
    m_normalBuffer3.bind();
    m_normalBuffer3.allocate(m_normal3.data(), m_normal3.size() * sizeof(float));}

void MultipleInstanceScene::prepareVertexArrayObjects()
{
    QOpenGLShaderProgramPtr shader = m_cube->shaderManager()->shader();
    shader->bind();

    m_cube->vertexArrayObject()->bind();
    m_modelViewBuffer1.bind();
    GLuint mvLocation = shader->attributeLocation("modelViewMatrix");
    for (GLuint i = 0; i < 4; ++i)
    {
        shader->enableAttributeArray(mvLocation+i);
        shader->setAttributeArray(mvLocation+i, GL_FLOAT
                                 ,(void*)(sizeof(float) * 4 * i)
                                 ,4
                                 ,sizeof(float) * 16);
        m_funcs->glVertexAttribDivisor(mvLocation+i, 1);
    }
    m_normalBuffer1.bind();
    GLuint normLocation = shader->attributeLocation("normalMatrix");
    for (GLuint i = 0; i < 3; ++i)
    {
        shader->enableAttributeArray(normLocation+i);
        shader->setAttributeArray(normLocation+i, GL_FLOAT
                                 ,(void*)(sizeof(float) * 3 * i)
                                 ,3
                                 ,sizeof(float) * 9);
        m_funcs->glVertexAttribDivisor(normLocation+i, 1);
    }
    m_cube->vertexArrayObject()->release();

    m_sphere->vertexArrayObject()->bind();
    m_modelViewBuffer2.bind();
    for (GLuint i = 0; i < 4; ++i)
    {
        shader->enableAttributeArray(mvLocation+i);
        shader->setAttributeArray(mvLocation+i, GL_FLOAT
                                 ,(void*)(sizeof(float) * 4 * i)
                                 ,4
                                 ,sizeof(float) * 16);
        m_funcs->glVertexAttribDivisor(mvLocation+i, 1);
    }
    m_normalBuffer2.bind();
    for (GLuint i = 0; i < 3; ++i)
    {
        shader->enableAttributeArray(normLocation+i);
        shader->setAttributeArray(normLocation+i, GL_FLOAT
                                 ,(void*)(sizeof(float) * 3 * i)
                                 ,3
                                 ,sizeof(float) * 9);
        m_funcs->glVertexAttribDivisor(normLocation+i, 1);
    }
    m_sphere->vertexArrayObject()->release();

    m_cylinder->vertexArrayObject()->bind();
    m_modelViewBuffer3.bind();
    for (GLuint i = 0; i < 4; ++i)
    {
        shader->enableAttributeArray(mvLocation+i);
        shader->setAttributeArray(mvLocation+i, GL_FLOAT
                                 ,(void*)(sizeof(float) * 4 * i)
                                 ,4
                                 ,sizeof(float) * 16);
        m_funcs->glVertexAttribDivisor(mvLocation+i, 1);
    }
    m_normalBuffer3.bind();
    for (GLuint i = 0; i < 3; ++i)
    {
        shader->enableAttributeArray(normLocation+i);
        shader->setAttributeArray(normLocation+i, GL_FLOAT
                                 ,(void*)(sizeof(float) * 3 * i)
                                 ,3
                                 ,sizeof(float) * 9);
        m_funcs->glVertexAttribDivisor(normLocation+i, 1);
    }
    m_cylinder->vertexArrayObject()->release();
}

