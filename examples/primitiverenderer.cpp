/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of %PROJECT%
 */

#include "primitiverenderer.h"

#include <QtQuick/QSGSimpleTextureNode>
#include <QtGui/QOpenGLContext>

#include <QtCore/QDebug>

#include "fbotexturenode.h"
#include "primitivesscene.h"
#include "instancedhistogramscene.h"
#include "multipleinstancescene.h"
#include "instancedprimitivesscene.h"
#include "managedscene.h"

PrimitiveRenderer::PrimitiveRenderer(QQuickItem* parent)
    : QQuickItem(parent)
    , m_scene(nullptr)
{
    setFlag(ItemHasContents, true);
}

PrimitiveRenderer::~PrimitiveRenderer()
{
    delete m_scene;
}

QSGNode*PrimitiveRenderer::updatePaintNode(QSGNode* oldNode, QQuickItem::UpdatePaintNodeData*)
{
    // Don't bother with resize and such, just recreate the node from scratch
    // when geometry changes.
    if ( oldNode )
        delete oldNode;

    // Create the scene we will render and allow it to initialise resources
    if ( !m_scene )
    {
        QOpenGLContext* context = QOpenGLContext::currentContext();
//        m_scene = new PrimitivesScene();
//        m_scene = new InstancedHistogramScene();
//        m_scene = new MultipleInstanceScene();
//        m_scene = new InstancedPrimitivesScene();
        m_scene = new ManagedScene();
        m_scene->setContext( context );
        m_scene->initialize();
    }

    // Create a new FboTextureNode and set scene on it
    qDebug() << "Recreating FboTextureNode";
    FboTextureNode* node = new FboTextureNode( window() );
    node->setScene( m_scene );
    node->setRect( boundingRect() );

    return node;
}

