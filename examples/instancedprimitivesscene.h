/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of %PROJECT%
 */

#ifndef INSTANCEDPRIMITIVESSCENE_H
#define INSTANCEDPRIMITIVESSCENE_H

#include "bopenglabstractscene.h"

class BOpenGLPrimitiveInstancingManager;
class BOpenGLCamera;

class BOpenGLPlanePrimitive;

class InstancedPrimitivesScene : public BOpenGLAbstractScene
{
    Q_OBJECT
public:
    explicit InstancedPrimitivesScene(QObject *parent = 0);
    ~InstancedPrimitivesScene();

    virtual void initialize();
    virtual void update(float t);
    virtual void render();
    virtual void resize(int w, int h);

private:
    BOpenGLPrimitiveInstancingManager * m_primMgr;
    int m_cubeId;
    int m_sphereId;
    int m_cylinderId;

    BOpenGLPlanePrimitive * m_ground;

    float m_theta;

    BOpenGLCamera * m_camera;
};

#endif // INSTANCEDPRIMITIVESSCENE_H
