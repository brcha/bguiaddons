/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of %PROJECT%
 */

#include "instancedprimitivesscene.h"

#include "bopenglcamera.h"
#include "bopenglshadermanager.h"
#include "bopenglcubeprimitive.h"
#include "bopenglsphereprimitive.h"
#include "bopenglcylinderprimitive.h"
#include "bopenglplaneprimitive.h"
#include "bopenglprimitiveinstancingmanager.h"

#include <QtGui/QOpenGLContext>

#include <math.h>

static const int pointCount = 400;

InstancedPrimitivesScene::InstancedPrimitivesScene(QObject *parent)
    : BOpenGLAbstractScene(parent)
    , m_primMgr(nullptr)
    , m_cubeId(-1)
    , m_sphereId(-1)
    , m_cylinderId(-1)
    , m_ground(nullptr)
    , m_theta(0.0f)
    , m_camera(new BOpenGLCamera(this))
{
    m_camera->setInQml(true);
    m_camera->setPosition(QVector3D(0.0f, 0.0f, 20.0f));
    m_camera->setViewCenter(QVector3D(0.0f, 0.0f, 0.0f));
    m_camera->setUpVector(QVector3D(0.0f, 1.0f, 0.0f));
}

InstancedPrimitivesScene::~InstancedPrimitivesScene()
{
}

void InstancedPrimitivesScene::initialize()
{
    BOpenGLShaderManagerPtr shader(new BOpenGLShaderManager);
    shader->setShaders(":/shaders/instancingmgr.vert.glsl"
                      ,":/shaders/instancingmgr.frag.glsl");

    m_primMgr = new BOpenGLPrimitiveInstancingManager(this);
    m_primMgr->setContext(m_context);
    m_primMgr->initialize();
    m_primMgr->setShaderManager(shader);

    BOpenGLCubePrimitive * cube = new BOpenGLCubePrimitive(this);
    cube->setShaderManager(shader);
    cube->create();
    m_cubeId = m_primMgr->addPrimitive(cube);

    BOpenGLSpherePrimitive * sphere = new BOpenGLSpherePrimitive(this);
    sphere->setShaderManager(shader);
    sphere->create();
    m_sphereId = m_primMgr->addPrimitive(sphere);

    BOpenGLCylinderPrimitive * cylinder = new BOpenGLCylinderPrimitive(this);
    cylinder->setShaderManager(shader);
    cylinder->create();
    m_cylinderId = m_primMgr->addPrimitive(cylinder);

    BOpenGLShaderManagerPtr planeShader(new BOpenGLShaderManager);
    planeShader->setShaders(":/shaders/shader.vert.glsl"
                           ,":/shaders/shader.frag.glsl");
    m_ground = new BOpenGLPlanePrimitive(50.0f, 50.0f, 100, 100, this);
    m_ground->setShaderManager(planeShader);
    m_ground->create();

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

    update(0.0f);
}

void InstancedPrimitivesScene::update(float t)
{
    const float xMin = -150.0f, xMax = 150.0f;
    const float dx = (xMax - xMin) / static_cast<float>(pointCount - 1);

    m_theta += 0.1f;

    QVector<QMatrix4x4> cubeModels(pointCount);
    QVector<QVector3D> cubeColors(pointCount);
    QVector<QMatrix4x4> sphereModels(pointCount);
    QVector<QVector3D> sphereColors(pointCount);
    QVector<QMatrix4x4> cylinderModels(pointCount);
    QVector<QVector3D> cylinderColors(pointCount);

    for (int i=0; i < pointCount; ++i)
    {
        float x = xMin + static_cast<float>(i) * dx;
        float y1 = 6.0f * sinf(0.4f * x - 0.4f * t);
        float y2 = 4.0f * cosf(0.7f * x - 0.2f * t);
        float y3 = 7.0f * cosf(0.3f * x - 0.8f * t);
        float col = 0.5f + 0.5f * cosf(0.6f * x - 0.1f * t);

        QMatrix4x4 cubeModelMatrix;
        cubeModelMatrix.setToIdentity();
        cubeModelMatrix.rotate(m_theta, 0.0f, 1.0f, 0.0f);
        cubeModelMatrix.translate(x, y1, 0.0f);

        cubeModels[i] = cubeModelMatrix;
        cubeColors[i] = QVector3D(0.0f, col, 0.1f); //QVector3D(0.0f, 0.6f, 0.1f);

        QMatrix4x4 sphereModelMatrix;
        sphereModelMatrix.setToIdentity();
        sphereModelMatrix.rotate(2*m_theta, 0.0f, 1.0f, 0.0f);
        sphereModelMatrix.translate(x, y2, 0.0f);

        sphereModels[i] = sphereModelMatrix;
        sphereColors[i] = QVector3D(col, 0.0f, 0.1f); //QVector3D(0.9f, 0.0f, 0.0f);

        QMatrix4x4 cylinderModelMatrix;
        cylinderModelMatrix.setToIdentity();
        cylinderModelMatrix.rotate(m_theta / 2.0f, 0.0f, 1.0f, 0.0f);
        cylinderModelMatrix.translate(x, y3, 0.0f);

        cylinderModels[i] = cylinderModelMatrix;
        cylinderColors[i] = QVector3D(1.0f - col, 0.3f, 0.6f); //QVector3D(0.9f, 0.3f, 0.6f);
    }

    m_primMgr->updatePrimitive(m_cubeId, cubeModels, cubeColors, m_camera);
    m_primMgr->updatePrimitive(m_sphereId, sphereModels, sphereColors, m_camera);
    m_primMgr->updatePrimitive(m_cylinderId, cylinderModels, cylinderColors, m_camera);
}

void InstancedPrimitivesScene::render()
{
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    m_primMgr->shaderManager()->bind();
    QOpenGLShaderProgramPtr shader = m_primMgr->shaderManager()->shader();

    shader->setUniformValue( "projectionMatrix", m_camera->projectionMatrix() );

    // Setup lighting
    shader->setUniformValue( "light.position", QVector4D( 0.0f, 0.0f, 0.0f, 1.0f ) );
    shader->setUniformValue( "light.intensity", QVector3D( 1.0f, 1.0f, 1.0f ) );
    shader->setUniformValue( "material.kd", QVector3D( 0.0f, 0.6f, 0.1f ) );
    shader->setUniformValue( "material.ks", QVector3D( 0.95f, 0.95f, 0.95f ) );
    shader->setUniformValue( "material.ka", QVector3D( 0.1f, 0.1f, 0.1f ) );
    shader->setUniformValue( "material.shininess", 100.0f );

    // Render primitives
    m_primMgr->render();

    // Draw plane
    m_ground->shaderManager()->bind();
    shader = m_ground->shaderManager()->shader();

    shader->setUniformValue( "projectionMatrix", m_camera->projectionMatrix() );

    // Setup lighting
    shader->setUniformValue( "light.position", QVector4D( 0.0f, 0.0f, 0.0f, 1.0f ) );
    shader->setUniformValue( "light.intensity", QVector3D( 1.0f, 1.0f, 1.0f ) );
    shader->setUniformValue( "material.kd", QVector3D(0.0f, 0.1f, 0.9f) );
    shader->setUniformValue( "material.ks", QVector3D( 0.95f, 0.95f, 0.95f ) );
    shader->setUniformValue( "material.ka", QVector3D( 0.1f, 0.1f, 0.1f ) );
    shader->setUniformValue( "material.shininess", 100.0f );


    QMatrix4x4 planeModel;
    planeModel.setToIdentity();
    planeModel.translate(0.0f, -7.0f, 0.0f);
    m_camera->setStandardUniforms(shader, planeModel);
    m_ground->render();
}

void InstancedPrimitivesScene::resize(int w, int h)
{
    glViewport(0, 0, w, h);

    float aspect = static_cast<float>(w) / static_cast<float>(h);
    m_camera->setPerspectiveProjection(60.0f, aspect, 0.3f, 1000.0f);
}

