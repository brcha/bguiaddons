/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * ***** END LICENSE BLOCK ***** */

/*
 * Copyright (c) 2015, Filip Brcic <brcha@gna.org>. All rights reserved.
 *
 * This file is part of %PROJECT%
 */

#ifndef MULTIPLEINSTANCESCENE_H
#define MULTIPLEINSTANCESCENE_H

#include "bopenglabstractscene.h"

#include <QtGui/QOpenGLBuffer>
#include <QtGui/QMatrix4x4>
#include <QtGui/QMatrix3x3>

class QOpenGLFunctions_3_3_Core;

class BOpenGLCamera;

class BOpenGLSpherePrimitive;
class BOpenGLCubePrimitive;
class BOpenGLPlanePrimitive;
class BOpenGLCylinderPrimitive;

class MultipleInstanceScene : public BOpenGLAbstractScene
{
    Q_OBJECT
public:
    explicit MultipleInstanceScene(QObject *parent = 0);
    ~MultipleInstanceScene();

    virtual void initialize();
    virtual void update(float t);
    virtual void render();
    virtual void resize(int w, int h);

    void toggleUpdates();

    // Camera motion control
    void setSideSpeed(float vx);
    void setVerticalSpeed(float vy);
    void setForwardSpeed(float vz);
    void setViewCenterFixed(bool fixed);

    // Camera orientation control
    void pan(float angle);
    void tilt(float angle);

private:
    void prepareVertexBuffers();
    void prepareVertexArrayObjects();

private:
    QOpenGLFunctions_3_3_Core * m_funcs;

    BOpenGLCamera * m_camera;
    float m_vx;
    float m_vy;
    float m_vz;
    bool m_viewCenterFixed;
    float m_panAngle;
    float m_tiltAngle;

    bool m_updatesEnabled;

    BOpenGLCubePrimitive * m_cube;

    BOpenGLSpherePrimitive * m_sphere;

    BOpenGLPlanePrimitive * m_plane;

    BOpenGLCylinderPrimitive * m_cylinder;

    float m_theta;

    // Data and buffers
    QVector<float> m_modelView1;
    QVector<float> m_normal1;
    QOpenGLBuffer m_modelViewBuffer1;
    QOpenGLBuffer m_normalBuffer1;
    QVector<float> m_modelView2;
    QVector<float> m_normal2;
    QOpenGLBuffer m_modelViewBuffer2;
    QOpenGLBuffer m_normalBuffer2;
    QVector<float> m_modelView3;
    QVector<float> m_normal3;
    QOpenGLBuffer m_modelViewBuffer3;
    QOpenGLBuffer m_normalBuffer3;
};

#endif // MULTIPLEINSTANCESCENE_H
